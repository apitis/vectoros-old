# Makefile for Vector

GCC := arm-none-eabi-gcc
OBJCOPY := arm-none-eabi-objcopy
OBJDUMP := arm-none-eabi-objdump
SIZE := arm-none-eabi-size
CPP := arm-none-eabi-cpp

SHELL := bash
RM := rm -rf

DEFINES := -D__SAM4S16B__ -DBOARD=VECTOR_P1

OPTIONS := -mthumb -O1 -fdata-sections -ffunction-sections -mlong-calls -g3 -Wall -c -std=gnu99   -mcpu=cortex-m4

OBJS +=  \
src/asf/common/services/clock/sam4s/sysclk.o \
src/asf/common/utils/interrupt/interrupt_sam_nvic.o \
src/asf/common/utils/stdio/read.o \
src/asf/common/utils/stdio/write.o \
src/asf/sam/drivers/pio/pio.o \
src/asf/sam/drivers/pio/pio_handler.o \
src/asf/sam/drivers/pmc/pmc.o \
src/asf/sam/drivers/spi/spi.o \
src/asf/sam/drivers/twi/twi.o \
src/asf/sam/drivers/rtc/rtc.o \
src/asf/sam/drivers/rtt/rtt.o \
src/asf/sam/drivers/adc/adc.o \
src/asf/sam/drivers/uart/uart.o \
src/asf/sam/drivers/usart/usart.o \
src/asf/sam/drivers/efc/efc.o \
src/asf/sam/services/flash_efc/flash_efc.o \
src/asf/sam/utils/cmsis/sam4s/source/templates/gcc/startup_sam4s.o \
src/asf/sam/utils/cmsis/sam4s/source/templates/system_sam4s.o \
src/asf/sam/utils/syscalls/gcc/syscalls.o \
src/main.o \
src/libraries/Nordic_BLE/aci_setup.o \
src/libraries/Nordic_BLE/acilib.o \
src/libraries/Nordic_BLE/lib_aci.o \
src/libraries/Nordic_BLE/hal_aci_tl.o \
src/libraries/SharpLCD/sharp_screen.o \
src/libraries/SharpLCD/16hHelvetica.o \
src/libraries/SharpLCD/32hHelvetica.o \
src/libraries/SharpLCD/Colaborate_Thin42h.o \
src/libraries/SharpLCD/WeatherImages24h.o \
src/libraries/MPU9150/MPU9150.o \
src/libraries/i2c/i2c.o \
src/libraries/flash/flash.o \
src/libraries/bluetooth/bluetooth.o \
src/libraries/pedometer/pedometer.o \
src/libraries/bluetooth/queue.o \
src/asf/common/services/delay/sam/cycle_counter.o

INCLUDES := -I"src/asf/common/boards" \
-I"src/asf/sam/boards/vector_p1" \
-I"src/asf/sam/utils/cmsis/sam4s/include" \
-I"src/asf/sam/utils/header_files" \
-I"src/asf/sam/utils/preprocessor" \
-I"src/asf/common/utils" \
-I"src/asf/sam/utils" \
-I"src/asf/thirdparty/CMSIS/Include" \
-I"src/asf/common/services/delay" \
-I"src/asf/common/services/serial" \
-I"src/asf/common/utils/stdio/stdio_serial" \
-I"src/asf/sam/drivers/usart" \
-I"src/asf/common/services/clock" \
-I"src/config"

# Add the include directories for all the objs
INCLUDES += $(foreach INC,$(dir $(OBJS)),-I$(INC))

# Generate the list of sources from the list of objs
C_SRCS = $(OBJS:.o=.c)

# All Target
all: VectorOS.elf

VectorOS.elf: $(OBJS) 
	arm-none-eabi-gcc -oVectorOS.elf $(OBJS) $(LIBS) -Wl,-Map="VectorOS.map" -Wl,--start-group -lm  -Wl,--end-group -L"../cmsis/linkerScripts"  -Wl,--gc-sections -Tsrc/asf/sam/utils/linker_scripts/sam4s/sam4s16/gcc/flash.ld -Wl,--cref -Wl,--entry=Reset_Handler -mthumb  -mcpu=cortex-m3 
	@echo Finished building target: $@
	arm-none-eabi-objcopy -O binary "VectorOS.elf" "VectorOS.bin"
	arm-none-eabi-objcopy -O ihex -R .eeprom -R .fuse -R .lock -R .signature  "VectorOS.elf" "VectorOS.hex"
	arm-none-eabi-objcopy -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O binary "VectorOS.elf" "VectorOS.eep" || exit 0
	arm-none-eabi-objdump -h -S "VectorOS.elf" > "VectorOS.lss"
	arm-none-eabi-size "VectorOS.elf"

.c.o:
	arm-none-eabi-gcc $(INCLUDES) $(DEFINES) $(OPTIONS) -o"$@" "$<" 


# Include dependencies of header files.
# Remember to run make dep when you change
# the includes in any /c file!!
include makedep.mk


# Other Targets
install:
	cat write_image | nc localhost 4444

clean:
	-$(RM) $(OBJS) $(EXECUTABLES) 
	rm -rf "VectorOS.elf" "VectorOS.a" "VectorOS.hex" "VectorOS.bin" "VectorOS.lss" "VectorOS.eep" "VectorOS.map"


dep:
	(for i in $(C_SRCS);\
        do\
                $(CPP) -mthumb $(DEFINES) $(INCLUDES) -MM -MT `echo $$i | sed s/[.]c/.o/` $$i;\
        done) > makedep.mk

tags:
	-rm TAGS
	find . -name '*.c' -exec etags -a {} \;
	find . -name '*.h' -exec etags -a {} \;


