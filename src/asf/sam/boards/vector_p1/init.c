/**
 * \file
 *
 * \brief SAM4S-EK board init.
 *
 *
 */

#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "pio.h"
//#include "ioport.h"
#include "conf_uart_serial.h"

