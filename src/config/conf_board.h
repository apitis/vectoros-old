/**
 * \file
 *
 * \brief Board configuration.
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef CONF_BOARD_H_INCLUDED
#define CONF_BOARD_H_INCLUDED

/** Name of the board */
#define BOARD_NAME "VECTOR-P1"
/** Board definition */
#define sam4s
/** Core definition */
#define cortexm4

/*----------------------------------------------------------------------------*/
/** PULSE pin definition. */
#define PIN_PULSE   {PIO_PA27, PIOA, ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_PULSE_MASK PIO_PA27
#define PIN_PULSE_PIO PIOA
#define PIN_PULSE_ID ID_PIOA
#define PIN_PULSE_TYPE PIO_OUTPUT_1
#define PIN_PULSE_ATTR PIO_DEFAULT

#define PULSE_GPIO     (PIO_PA27_IDX)
#define PULSE_FLAGS    (PIO_OUTPUT_1 | PIO_DEFAULT)
#define PULSE_ACTIVE_LEVEL 1

#define PIN_RDYN    {PIO_PA25, PIOA, ID_PIOA, PIO_INPUT,  PIO_IT_LOW_LEVEL}
#define PIN_RDYN_MASK PIO_PA25
#define PIN_RDYN_PIO PIOA
#define PIN_RDYN_ID ID_PIOA
#define PIN_RDYN_TYPE PIO_INPUT
#define PIN_RDYN_ATTR PIO_PULLUP | PIO_IT_LOW_LEVEL

/** Push button #0 definition. Attributes = pull-up + debounce + interrupt on rising edge. */
#define PUSHBUTTON_1_NAME    "USRPB1"
#define GPIO_PUSH_BUTTON_1   (PIO_PA0_IDX)
#define GPIO_PUSH_BUTTON_1_FLAGS    (PIO_INPUT | PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)

#define PIN_PUSHBUTTON_1    {PIO_PA0, PIOA, ID_PIOA, PIO_INPUT, PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE}
#define PIN_PUSHBUTTON_1_MASK PIO_PA0
#define PIN_PUSHBUTTON_1_PIO PIOA
#define PIN_PUSHBUTTON_1_ID ID_PIOA
#define PIN_PUSHBUTTON_1_TYPE PIO_INPUT
#define PIN_PUSHBUTTON_1_ATTR PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE

/** Push button #1 definition. Attributes = pull-up + debounce + interrupt on falling edge. */
#define PUSHBUTTON_2_NAME    "USRPB2"
#define GPIO_PUSH_BUTTON_2   (PIO_PA1_IDX)
#define GPIO_PUSH_BUTTON_2_FLAGS    (PIO_INPUT | PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_FALL_EDGE)

#define PIN_PUSHBUTTON_2    {PIO_PA1, PIOA, ID_PIOA, PIO_INPUT, PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_FALL_EDGE}
#define PIN_PUSHBUTTON_2_MASK PIO_PA1
#define PIN_PUSHBUTTON_2_PIO PIOA
#define PIN_PUSHBUTTON_2_ID ID_PIOA
#define PIN_PUSHBUTTON_2_TYPE PIO_INPUT
#define PIN_PUSHBUTTON_2_ATTR PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_FALL_EDGE

/** Push button #2 definition. Attributes = pull-up + debounce + interrupt on falling edge. */
#define PUSHBUTTON_3_NAME    "USRPB3"
#define GPIO_PUSH_BUTTON_3   (PIO_PA2_IDX)
#define GPIO_PUSH_BUTTON_3_FLAGS    (PIO_INPUT | PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_FALL_EDGE)

#define PIN_PUSHBUTTON_3    {PIO_PA2, PIOA, ID_PIOA, PIO_INPUT, PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_FALL_EDGE}
#define PIN_PUSHBUTTON_3_MASK PIO_PA2
#define PIN_PUSHBUTTON_3_PIO PIOA
#define PIN_PUSHBUTTON_3_ID ID_PIOA
#define PIN_PUSHBUTTON_3_TYPE PIO_INPUT
#define PIN_PUSHBUTTON_3_ATTR PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_FALL_EDGE

/** List of all push button definitions. */
#define PINS_PUSHBUTTONS    PIN_PUSHBUTTON_1, PIN_PUSHBUTTON_2, PIN, PUSHBUTTON_3

/** Shaker.  **/
#define SHAKER_ENABLE    (PIO_PA24_IDX)

/** Specific pins for the Nordic BLE 8001 controller.  */
#define BLE_REQN         (PIO_PA26_IDX)
#define BLE_RDNY         (PIO_PA25_IDX)

/** Specific pins for the Sharp Memory LCD Display.  */
#define SHARP_DISP       (PIO_PA10_IDX)
#define SHARP_EXTC       (PIO_PA15_IDX)
#define SHARP_SCS        (PIO_PA11_IDX)

/** SPI MISO, MOSI, SPCK pin definition. */
#define SPI_MISO_GPIO        (PIO_PA12_IDX)
#define SPI_MISO_FLAGS       (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI_MOSI_GPIO        (PIO_PA13_IDX)
#define SPI_MOSI_FLAGS       (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI_SPCK_GPIO        (PIO_PA14_IDX)
#define SPI_SPCK_FLAGS       (PIO_PERIPH_A | PIO_DEFAULT)

/** SPI chip select 0 pin definition. (Only one configuration is possible) */
#define SPI_NPCS0_GPIO            (PIO_PA11_IDX)
#define SPI_NPCS0_FLAGS           (PIO_PERIPH_A | PIO_DEFAULT)

/** USART0 pin RX **/
#define PIN_USART0_RXD    {PIO_PA5A_RXD0, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_RXD_IDX        (PIO_PA5_IDX)
#define PIN_USART0_RXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)
/** USART0 pin TX */
#define PIN_USART0_TXD    {PIO_PA6A_TXD0, PIOA, ID_PIOA, PIO_PERIPH_A, PIO_DEFAULT}
#define PIN_USART0_TXD_IDX        (PIO_PA6_IDX)
#define PIN_USART0_TXD_FLAGS      (PIO_PERIPH_A | PIO_DEFAULT)


/** TWI0 pins definition */
#define TWI0_DATA_GPIO   PIO_PA3_IDX
#define TWI0_DATA_FLAGS  (PIO_PERIPH_A | PIO_DEFAULT)
#define TWI0_CLK_GPIO    PIO_PA4_IDX
#define TWI0_CLK_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)

////////////////////////////////////////////////////////////

/** Usart Hw ID used by the console (UART0). */
#define CONSOLE_UART_ID	 ID_USART0		/** Usart Hw ID used by the console (UART1). */
#define CONSOLE_UART     USART0

/** SPI MACRO definition */
#define CONF_BOARD_SPI

/** Spi Hw ID . */
#define SPI_ID          ID_SPI

/** SPI base address for SPI master mode*/
#define SPI_MASTER_BASE      SPI

#define PLLA_COUNT						0x3Fu

#define AD4                                                             PIO_PB0_IDX
#define AD5                                                             PIO_PB1_IDX
#define AD6                                                             PIO_PB2_IDX
#define AD7                                                             PIO_PB3_IDX
#define ADC_FLAGS                                               (PIO_INPUT | PIO_DEFAULT)

#define IFLASH_PAGE_SIZE  IFLASH0_PAGE_SIZE

/* Last page start address. */
#define BOND_START_ADDRESS (IFLASH0_ADDR + IFLASH_SIZE - IFLASH_PAGE_SIZE * 16)

/** TWI frequency 400kHz */
#define TWI_SPEED 400000                  

#endif /* CONF_BOARD_H_INCLUDED */
