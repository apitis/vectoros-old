#include "hal_platform.h"

#include "asf.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "stdio_serial.h"
#include "pedometer.h"
#include <boards.h>
#include <bluetooth.h>
#include <services.h> 

#include "flash.h"
#include "MPU9150.h"
#include "lib_aci.h"
#include "sharp_screen.h"

#define IRQ_PRIOR_PIO    0

/* Chip select. */
#define SPI_CHIP_SEL 0

/* Clock polarity. */
#define SPI_CLK_POLARITY 0

/* Clock phase. */
#define SPI_CLK_PHASE 1

/* Delay before SPCK. */
#define SPI_DLYBS 0x40

/* Delay between consecutive transfers. */
#define SPI_DLYBCT 0x10

/* Number of SPI clock configurations. */
#define NUM_SPCK_CONFIGURATIONS 4

/* SPI Communicate buffer size. */
#define COMM_BUFFER_SIZE   64

/* UART baudrate. */
#define UART_BAUDRATE      115200

/* ADC Channel values. */
int adc_ch[32];

int g_ticks = 0;

/* BLE uses the SPI.  */
uint8_t BLE_SPI_lock = 0;

int bibi = 0;

int g_buzz = 0;

#define BUZZ_200ms 4

void do_buzz()
{
    g_buzz = BUZZ_200ms;
}

int n_enter_sleep = 0;
int doing_ADC = 0;


void goto_sleep()
{
    n_enter_sleep++;
    
    if (!doing_ADC && g_ticks > 10)
        pmc_switch_mck_to_mainck(SYSCLK_PRES_64);
          
    pmc_enable_sleepmode(0);
    pmc_switch_mck_to_mainck(SYSCLK_PRES_1);
}


/**
 * \brief Initialize SPI as master.
 */
static void spi_master_initialize(void)
{
	/* Configure an SPI peripheral. */
	pmc_enable_periph_clk(SPI_ID);
	spi_disable(SPI_MASTER_BASE);
	spi_reset(SPI_MASTER_BASE);
	spi_set_lastxfer(SPI_MASTER_BASE);
	spi_set_master_mode(SPI_MASTER_BASE);
	spi_disable_mode_fault_detect(SPI_MASTER_BASE);
	spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_CHIP_SEL);
	spi_set_clock_polarity(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_POLARITY);
	spi_set_clock_phase(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_PHASE);
	spi_set_bits_per_transfer(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(SPI_MASTER_BASE, SPI_CHIP_SEL, (sysclk_get_cpu_hz() / 1000000));
	spi_set_transfer_delay(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_DLYBS, SPI_DLYBCT);
	spi_enable(SPI_MASTER_BASE);
}



static int rdyn_tick = 0;


static void Button1_Handler(uint32_t id, uint32_t mask)
{
    if (PIN_PUSHBUTTON_1_ID == id && PIN_PUSHBUTTON_1_MASK == mask)
    {
        pio_set_pin_high(PULSE_GPIO);
    }
}

static void RDYN_Handler(uint32_t id, uint32_t mask)
{
    pmc_switch_mck_to_mainck(SYSCLK_PRES_1);
    
    if (PIN_RDYN_ID == id && PIN_RDYN_MASK == mask) {
        rdyn_tick++;

        BLE_SPI_lock = 1;
        m_rdy_line_handle();
        BLE_SPI_lock = 0;
    }
}

int ble_interrupt_attached = 0;

void attach_ble_interrupt()
{
    ble_interrupt_attached = 1;
    
    /* Configure RDYN */
    pmc_enable_periph_clk(PIN_RDYN_ID);

    /* Interrupt on low level */
    pio_handler_set(PIN_RDYN_PIO, PIN_RDYN_ID, PIN_RDYN_MASK, PIN_RDYN_ATTR, RDYN_Handler);
    NVIC_EnableIRQ((IRQn_Type) PIN_RDYN_ID);
    pio_handler_set_priority(PIN_RDYN_PIO, (IRQn_Type) PIN_RDYN_ID, IRQ_PRIOR_PIO);
    pio_enable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
}

uint8_t display_debug = 0;

static void Button2_Handler(uint32_t id, uint32_t mask)
{
    if (PIN_PUSHBUTTON_2_ID == id && PIN_PUSHBUTTON_2_MASK == mask)
    {
        pio_set_pin_low(PULSE_GPIO);

        display_debug = !display_debug;

        if (!display_debug)
        {
            screen_delete_lines(10, 50);
            screen_delete_lines(112, 16);
        }
    }
}


static void configure_buttons(void)
{
    /* Configure Pushbutton 1 */
    pmc_enable_periph_clk(PIN_PUSHBUTTON_1_ID);
    pio_set_debounce_filter(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK, 10);

    /* Interrupt on rising edge  */
    pio_handler_set(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_ID,
                    PIN_PUSHBUTTON_1_MASK, PIN_PUSHBUTTON_1_ATTR, Button1_Handler);
    NVIC_EnableIRQ((IRQn_Type) PIN_PUSHBUTTON_1_ID);
    pio_handler_set_priority(PIN_PUSHBUTTON_1_PIO,
                             (IRQn_Type) PIN_PUSHBUTTON_1_ID, IRQ_PRIOR_PIO);
    pio_enable_interrupt(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK);
    
    /* Configure Pushbutton 2 */
    pmc_enable_periph_clk(PIN_PUSHBUTTON_2_ID);
    pio_set_debounce_filter(PIN_PUSHBUTTON_2_PIO, PIN_PUSHBUTTON_2_MASK, 10);

    /* Interrupt on falling edge */
    pio_handler_set(PIN_PUSHBUTTON_2_PIO, PIN_PUSHBUTTON_2_ID,
                    PIN_PUSHBUTTON_2_MASK, PIN_PUSHBUTTON_2_ATTR, Button2_Handler);
    NVIC_EnableIRQ((IRQn_Type) PIN_PUSHBUTTON_2_ID);
    pio_handler_set_priority(PIN_PUSHBUTTON_2_PIO,
                             (IRQn_Type) PIN_PUSHBUTTON_2_ID, IRQ_PRIOR_PIO);
    pio_enable_interrupt(PIN_PUSHBUTTON_2_PIO, PIN_PUSHBUTTON_2_MASK);
}

void RTC_init(void)
{

        pmc_switch_sclk_to_32kxtal(PMC_OSC_XTAL);       /* Enable 32kHz external crystal */
        while (!pmc_osc_is_ready_32kxtal());            /* Wait for 32K oscillator ready */

        rtc_set_hour_mode(RTC, 0);                                      /* Default RTC configuration, 24-hour mode */

        /* Configure RTC interrupts */
        NVIC_DisableIRQ(RTC_IRQn);
        NVIC_ClearPendingIRQ(RTC_IRQn);
        NVIC_SetPriority(RTC_IRQn, 3);
        NVIC_EnableIRQ(RTC_IRQn);

        rtc_enable_interrupt(RTC, RTC_IER_SECEN | RTC_IER_ALREN); /* Interrupts for seconds and alerts */

}

/**
 * \brief RTT configuration function.
 *
 * Configure the RTT to generate a one second tick, which triggers the RTTINC
 * interrupt.
 */
static void configure_rtt(void)
{
    uint32_t ul_previous_time;

    rtt_sel_source(RTT, false);
    rtt_init(RTT, 32768 / 20);

    ul_previous_time = rtt_read_timer_value(RTT);
    while (ul_previous_time == rtt_read_timer_value(RTT));

    /* Enable RTT interrupt */
    NVIC_DisableIRQ(RTT_IRQn);
    NVIC_ClearPendingIRQ(RTT_IRQn);
    NVIC_SetPriority(RTT_IRQn, 0);
    NVIC_EnableIRQ(RTT_IRQn);
    rtt_enable_interrupt(RTT, RTT_MR_RTTINCIEN);
}


const static char *day_of_week[] = {"NA", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
const static char *month_name[] = {"NA", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

int _itoa(uint32_t x, char *s, int n)
{
    int i;

    for (i = n; i > 0; i--)
    {
        *(s+i-1) = (x % 10) + '0';
        x = x / 10;
    }

    return n;
}

int _itoa2(int x, char *s, int n)
{
    int i;

    for (i = n; i > 0; i--)
    {
        *(s+i-1) = (x % 10) + '0';
        x = x / 10;
    }

    while (*s == '0')
        *s++ = ' ';
    
    return n;
}

int _stoa(const char *x, char *s, int n)
{
    int i;

    for (i = n; i > 0; i--)
        *s++ = *x++;

    return n;

}

int v_batt_m[10];
int n_v_batt = 0;
int i_v_batt = 0;

int get_v_batt(int v_batt)
{
    int i, sum = 0;
    
    v_batt_m[i_v_batt] = v_batt;

    if (++i_v_batt >= 10)
        i_v_batt = 0;

    if (++n_v_batt >= 10)
        n_v_batt = 10;

    for (i = 0; i < n_v_batt; i++)
        sum += v_batt_m[i];

    return sum / n_v_batt;
}

extern int gr_index;
extern int gr_height;
int MPU9150_started = 0;
extern int16_t accl[3];

extern int ble_debug;

extern char notification_type[256];
extern int notification_length = 0;
extern char notification_details[256];
extern int nr_of_active_notifications;


int g_temp = 0;
int g_sign = 0;

void adc_sw_trigger()
{
    doing_ADC = 1;
    adc_start(ADC);
}

extern int min_grafic;
extern int max_grafic;

extern int g_nSteps;

void RTC_Handler(void)
{
    uint32_t ul_status = rtc_get_status(RTC);
    uint32_t ul_sec, ul_min, ul_hour;
    uint32_t ul_year, ul_month, ul_day, ul_week;
    char buf[1024];

    int v_batt, v_light;

    if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {                   /* Second increment interrupt */
        rtc_disable_interrupt(RTC, RTC_IDR_SECDIS);                     /* Disable RTC interrupt */

        g_ticks++;

        if (g_ticks == 3)
        {
            /* Turn LEDs off after a few seconds.  */
            pio_set_pin_low(PULSE_GPIO);

            MPU9150_setup();
            MPU9150_started = 1;
        }

        rtc_get_time(RTC,&ul_hour, &ul_min, &ul_sec);
        rtc_get_date(RTC,&ul_year, &ul_month, &ul_day, &ul_week);

        if (!display_debug)
        {
            strcpy(buf, "XX:XX");
            _itoa(ul_hour, buf, 2);
            _itoa(ul_min, buf + 3, 2);
            screen_put_bigtext_at(buf, -1, 10, THIN42);
        
            strcpy(buf, "XXX, XXX XX");   
            _stoa(day_of_week[ul_week], buf, 3);
            _stoa(month_name[ul_month], buf + 5, 3);
            _itoa(ul_day, buf + 9, 2);
            screen_put_bigtext_at(buf, -1, 44, HELV16);
        }
        
        strcpy(buf, "XX\200");
        _itoa(g_temp, buf, 2);
        screen_put_bigtext_at(buf, 30, 60, HELV32);
        strcpy(buf, "\200");
        buf[0] = g_sign & 0x80 ? g_sign : 0x80;
        screen_put_bigtext_at(buf, 70, 62, WEATHER);

        if (notification_type[0])
            screen_put_bigtext_at(notification_type, 8, 90, HELV16);
        else 
            screen_delete_lines(90, 16);
        
        if (notification_details[0])
            screen_put_bigtext_at(notification_details, 8, 105, HELV16);
        else
            screen_delete_lines(105, 16);

        strcpy(buf, "XXXXX");
        _itoa2(g_nSteps, buf, 5);
        screen_put_bigtext_at(buf, -2, 90, HELV16);

//            screen_logo();
        
/*         strcpy(buf, "HZ:XXXXXXXXXXXX"); */
/*         _itoa(accl[2], buf + 3, 12); */
/* //        _itoa(sysclk_get_cpu_hz(), buf + 3, 12); */
/*         screen_put_text_at(buf, 13, 0); */
        
        v_batt = (adc_ch[4] * 18810) >> 12;

        /* Moving average gets computed after the first couple of samples.  */
        if (g_ticks >= 5)
            v_batt = get_v_batt(v_batt);

        if (display_debug)
        {
            /* strcpy(buf, "XXX|X|XXX|"); */
            /* _itoa(v_batt / 10, buf, 3); */
            /* _itoa(ble_connected(), buf + 4, 1); */
            /* _itoa(get_n_enter_sleep(), buf + 6, 3); */

            strcpy(buf, "XXXXX|XXXXX");
            _itoa2(min_grafic, buf, 5);
            _itoa2(max_grafic, buf+6, 5);
            screen_put_text_at(buf, 15, 0);
        }

        screen_put_bigtext_at(ble_connected() ? "\007" : "\006", 0, 0, HELV16);

        /* Let the battery stabilize before displaying it.  */
        if (g_ticks > 60)
        {
            strcpy(buf, "---%");

            v_batt = (v_batt - 3700) * 100 / (4200 - 3700);
            if (v_batt < 0) v_batt = 0;
            if (v_batt > 100) v_batt = 100;
            _itoa2(v_batt, buf, 3);
            screen_put_text_at(buf, 0, 12);   
        
        }
        
        
/*         if (display_debug) */
/*         { */
/*             strcpy(buf, "[X][XXX][X][XXX]"); */
/* //        _itoa(rdyn_tick, buf + 1, 1); */
/*             _itoa(bibi, buf + 1, 1); */
/*             _itoa(get_n_enter_sleep(), buf + 4, 3); */
/*             _itoa(ble_connected(), buf + 9, 1); */
/* //        _itoa(gr_index, buf + 12, 3); */
/*             _itoa(ble_debug, buf + 12, 3); */
/*             screen_put_text_at(buf, 0, 0); */

        
/*         } */

        if (display_debug)
            draw_grafic(10, 48);
        
        refresh_screen();
                
        rtc_clear_status(RTC, RTC_SCCR_SECCLR);
        rtc_enable_interrupt(RTC, RTC_IER_SECEN);

    }else{
        if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM){                /* Time or date alarm */
            rtc_disable_interrupt(RTC, RTC_IDR_ALRDIS);                     /* Disable RTC interrupt */
            rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
            rtc_enable_interrupt(RTC, RTC_IER_ALREN);
        }
    }

    if (g_ticks < 60 || (g_ticks & 0x3F) == 0)
        adc_sw_trigger();
}

char IMU_buffer[16] = "";

/**
 * \brief Interrupt handler for the RTT.
 *
 * Display the current time on the terminal.
 */
void RTT_Handler(void)
{
    static int ticks = 0;
    uint32_t ul_status;
    char buf[50];
    
    /* Get RTT status */
    ul_status = rtt_get_status(RTT);

    pmc_switch_mck_to_mainck(SYSCLK_PRES_1);
    
    /* Time has changed, refresh display */
    if ((ul_status & RTT_SR_RTTINC) == RTT_SR_RTTINC)
    {
        uint32_t normalized;

        if (g_buzz)
        {
            pio_set_pin_high(SHAKER_ENABLE);
            g_buzz--;
        }
        else
            pio_set_pin_low(SHAKER_ENABLE);
            
        if (MPU9150_started)
        {
            MPU9150_readXYZ();

            normalized += accl[0] * accl[0];
            normalized += accl[1] * accl[1];
            normalized += accl[2] * accl[2];

            normalized = avg_filter(normalized >> 20, 5);

            add_grafic_pixel(normalized);
//            getSteps(normalized);

            FSM_input(normalized, ticks++);
        }
        

//        add_grafic_pixel(adc_ch[5]>>3);
        /* Start conversion.  */
//        adc_start(ADC);
    }
}


void ADC_init(void)
{
    pmc_enable_periph_clk(ID_ADC);

    adc_init(ADC, sysclk_get_cpu_hz(), 64000000, 8);

    /* Set resolution to 12bits => 4095.  */
    adc_set_resolution(ADC, ADC_MR_LOWRES_BITS_12);
    adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1);

    /* Tag enable for channel recognition.  */
    adc_enable_tag(ADC);

    /* Enable analog channels 4..5.  */
    adc_enable_channel(ADC, ADC_CHANNEL_4);
    adc_enable_channel(ADC, ADC_CHANNEL_5);

    /* Sleep between conversions.  */
    adc_configure_power_save(ADC, 1, 0);

    /* Enable software trigger.  */
    adc_configure_trigger(ADC, ADC_TRIG_SW, 0);	

    adc_disable_anch(ADC);

    /* Start conversion.  */
    adc_sw_trigger();

    /* enable interrupt for DRDY */
    adc_enable_interrupt(ADC, ADC_ISR_DRDY);

    /* enable ADC interrupts in NVIC */
    NVIC_DisableIRQ(ADC_IRQn);
    NVIC_ClearPendingIRQ(ADC_IRQn);
    NVIC_SetPriority(ADC_IRQn, 6);
    NVIC_EnableIRQ(ADC_IRQn);
}


void ADC_Handler(void)
{
    if ((adc_get_status(ADC) & ADC_ISR_DRDY) == ADC_ISR_DRDY)
    {
        int ul_temp = adc_get_latest_value(ADC);
        int uc_ch_num = (ul_temp & ADC_LCDR_CHNB_Msk) >> ADC_LCDR_CHNB_Pos;

        doing_ADC = 0;
        
        adc_ch[uc_ch_num] = ul_temp & ADC_LCDR_LDATA_Msk;
    }
}

extern int n_enter_sleep;



void TWI_init(void)
{
    pmc_enable_periph_clk(ID_TWI0);

    pio_configure_pin(PIO_PA3_IDX, PIO_PERIPH_A | PIO_DEFAULT);   // TWI0 data
    pio_configure_pin(PIO_PA4_IDX, PIO_PERIPH_A | PIO_DEFAULT);   // TWI0 clock

    twi_options_t    twi_options= {
        .master_clk = sysclk_get_cpu_hz(),
        .speed      = TWI_SPEED,
        .chip       = 0x80,
        .smbus      = 0
    };

    twi_enable_master_mode(TWI0);
    twi_master_init(TWI0, &twi_options);
}

int _atoi2(char *b)
{
    return (b[0] - '0') * 10 + (b[1] - '0');
}

void board_init(void)
{
#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
#endif

    pmc_enable_periph_clk(ID_PIOA);

    pio_configure_pin(PULSE_GPIO, PIO_OUTPUT_1);
    pio_configure_pin(SHAKER_ENABLE, PIO_OUTPUT_0);

    pio_set_input(PIOA, PIO_PA0, PIO_PULLUP);
    pio_set_input(PIOA, PIO_PA1, PIO_PULLUP);
    pio_set_input(PIOA, PIO_PA2, PIO_PULLUP);
    
    pio_configure_pin(SPI_MISO_GPIO, SPI_MISO_FLAGS);
    pio_configure_pin(SPI_MOSI_GPIO, SPI_MOSI_FLAGS);
    pio_configure_pin(SPI_SPCK_GPIO, SPI_SPCK_FLAGS);

    pio_configure_pin(SPI_NPCS0_GPIO, SPI_NPCS0_FLAGS);

    pio_configure_pin(AD4, ADC_FLAGS);
    pio_configure_pin(AD5, ADC_FLAGS);
    pio_configure_pin(AD6, ADC_FLAGS);
    pio_configure_pin(AD7, ADC_FLAGS);

    /* UART1 pin init */
//    pio_configure_pin_group(PINS_UART_PIO, PINS_UART, PINS_UART_FLAGS);
    pio_configure_pin(PIN_USART0_RXD_IDX, PIN_USART0_RXD_FLAGS);
    pio_configure_pin(PIN_USART0_TXD_IDX, PIN_USART0_TXD_FLAGS);
}


/* Entry point.  */
int main(void)
{
    int init_ble_int = 1;

    const usart_serial_options_t usart_serial_options = {
        .baudrate     = 115200,
        .charlength   = US_MR_CHRL_8_BIT,
        .paritytype   = US_MR_PAR_NO,
        .stopbits     = US_MR_NBSTOP_1_BIT
    };
    
    sysclk_init();
    board_init();

    stdio_serial_init(USART0, &usart_serial_options);
    

    /* Light up the LEDs.  */
    pio_set_pin_high(PULSE_GPIO);
//    pio_set_pin_low(SHAKER_ENABLE);
    
    spi_master_initialize();

    init_lcd_screen();

    RTC_init();

    /* Start BTLE.  */
    ble_begin();

    ADC_init();

    configure_buttons();

    configure_rtt();
    TWI_init();

    //EEPROM_write(0, 0);

    printf("Initialized\n\r");
    
    /* Main loop.  */
    while (1)
    {
        char buffer[1024], *b = buffer;
        
        while (ble_available())
            *b++ = ble_read();

        *b = '\0';

        if (b != buffer)
        {
            b = buffer + 1;

            switch (*buffer)
            {
                case 3:
                    /* Should set this from flash.  */
                    rtc_set_time(RTC, _atoi2(b), _atoi2(b + 3), _atoi2(b + 6));
                    rtc_set_date(RTC, 2014, _atoi2(b + 12), _atoi2(b + 9), b[15] - '0');

                    ble_write_text("Succes mult!\r");
                    break;
                    
                case 4:
                    g_temp = _atoi2(b);
                    g_sign = b[3] - '0' + 0x80;
                    break;

                case 5:
                    screen_put_text_at(b, 15, 10);
                    ble_write_text(b);
                    break;
            }
        }

        /* Will sleep if there's nothing to do.  Will be awakened by any interrupt. */
        ble_do_events();

        /* if (*IMU_buffer) */
        /* { */
        /*     ble_write_text(IMU_buffer); */
        /*     *IMU_buffer = 0; */
        /* } */
    }
        
    return 0;
}

