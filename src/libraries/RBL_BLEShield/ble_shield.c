
/*

Copyright (c) 2012, 2013 RedBearLab

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "hal_platform.h"
#include "asf.h"

#include <boards.h>
#include <lib_aci.h>
#include <aci_setup.h>

#include <ble_shield.h>

#include <delay.h>

#ifdef SERVICES_PIPE_TYPE_MAPPING_CONTENT
    static services_pipe_type_mapping_t
        services_pipe_type_mapping[NUMBER_OF_PIPES] = SERVICES_PIPE_TYPE_MAPPING_CONTENT;
#else
    #define NUMBER_OF_PIPES 0
    static services_pipe_type_mapping_t * services_pipe_type_mapping = NULL;
#endif

/* Store the setup for the nRF8001 in the flash of the AVR to save on RAM */
static hal_aci_data_t setup_msgs[NB_SETUP_MESSAGES] = SETUP_MESSAGES_CONTENT;

/*aci_struct that will contain :
total initial credits
current credit
current state of the aci (setup/standby/active/sleep)
open remote pipe pending
close remote pipe pending
Current pipe available bitmap
Current pipe closed bitmap
Current connection interval, slave latency and link supervision timeout
Current State of the the GATT client (Service Discovery)
Status of the bond (R) Peer address*/
static struct aci_state_t aci_state;

/*Temporary buffers for sending ACI commands*/
static hal_aci_evt_t  aci_data;
//static hal_aci_data_t aci_cmd;

/*Timing change state variable*/
static bool timing_change_done = false;

int n_enter_sleep = 0;

int get_n_enter_sleep()
{
    return n_enter_sleep;
}

/*Initialize the radio_ack. This is the ack received for every transmitted packet.*/
//static bool radio_ack_pending = false;

//The maximum size of a packet is 64 bytes.
#define MAX_TX_BUFF 64
static uint8_t tx_buff[MAX_TX_BUFF];
uint8_t tx_buffer_len = 0;

#define MAX_RX_BUFF 64

uint8_t rx_buff[MAX_RX_BUFF+1];
uint8_t rx_buffer_len = 0;
uint8_t *p_before = &rx_buff[0] ;
uint8_t *p_back = &rx_buff[0];
static unsigned char is_connected = 0;

// VECTOR-P1
static uint8_t reqn_pin = PIO_PA26_IDX, rdyn_pin = PIO_PA25_IDX;
static uint8_t MISO = PIO_PA12_IDX, MOSI = PIO_PA13_IDX, SCK = PIO_PA14_IDX;

void ble_interrupt_mode_true()
{
    aci_state.aci_pins.interface_is_interrupt	  = true;
}

void ble_begin()
{
    /* Point ACI data structures to the the setup data that the nRFgo studio generated for the nRF8001 */   
    if (NULL != services_pipe_type_mapping)
    {
        aci_state.aci_setup_info.services_pipe_type_mapping = &services_pipe_type_mapping[0];
    }
    else
    {
        aci_state.aci_setup_info.services_pipe_type_mapping = NULL;
    }
    
    aci_state.aci_setup_info.number_of_pipes    = NUMBER_OF_PIPES;
    aci_state.aci_setup_info.setup_msgs         = setup_msgs;
    aci_state.aci_setup_info.num_setup_msgs     = NB_SETUP_MESSAGES;

    /*
      Tell the ACI library, the MCU to nRF8001 pin connections.
      The Active pin is optional and can be marked UNUSED
    */	  	
    aci_state.aci_pins.board_name = REDBEARLAB_SHIELD_V1_1; //See board.h for details
    aci_state.aci_pins.reqn_pin   = reqn_pin;
    aci_state.aci_pins.rdyn_pin   = rdyn_pin;
    aci_state.aci_pins.mosi_pin   = MOSI;
    aci_state.aci_pins.miso_pin   = MISO;
    aci_state.aci_pins.sck_pin    = SCK;
	
    aci_state.aci_pins.spi_clock_divider     = 84;
	  
    aci_state.aci_pins.interface_is_interrupt	  = false;

    //Turn debug printing on for the ACI Commands and Events to be printed on the Serial
    lib_aci_debug_print(true);

    /*We reset the nRF8001 here by toggling the RESET line connected to the nRF8001
      and initialize the data structures required to setup the nRF8001*/

    lib_aci_init(&aci_state); 
    delay_ms(200);
}

volatile uint8_t ack = 0;

void ble_write(unsigned char data)
{	    
    if(tx_buffer_len == MAX_TX_BUFF)
    {
        return;
    }	
    tx_buff[tx_buffer_len] = data;
    tx_buffer_len++;	
		
}

void ble_write_bytes(unsigned char *data, uint8_t len)
{
    for (int i = 0; i < len; i++)
        ble_write(data[i]);
}

void ble_write_text(unsigned char *data)
{
    if (is_connected)
        ble_write_bytes(data, strlen(data));
}

int ble_read()
{
	int data;
	if(rx_buffer_len == 0) return -1;
	if(p_before == &rx_buff[MAX_RX_BUFF])
	{
			p_before = &rx_buff[0];
	}
	data = *p_before;
	p_before ++;
	rx_buffer_len--;
	return data;
}

unsigned char ble_available()
{
	return rx_buffer_len;
}

unsigned char ble_connected()
{
    return is_connected;
}

static int cmd_status = 0;
int ble_cmd_status()
{
    return cmd_status;
}

int init_ble_int = 1;

static void process_events()
{
    // We enter the if statement only when there is a ACI event available to be processed
    if (lib_aci_event_get(&aci_state, &aci_data))
    {
        aci_evt_t  *aci_evt;   
        aci_evt = &aci_data.evt;    

        switch(aci_evt->evt_opcode)
        {
            /* As soon as you reset the nRF8001 you will get an ACI Device Started Event */
            case ACI_EVT_DEVICE_STARTED:
                aci_state.data_credit_total = aci_evt->params.device_started.credit_available;
							
                switch(aci_evt->params.device_started.device_mode)
                {
                    case ACI_DEVICE_SETUP:
                        /* When the device is in the setup mode*/
                        //FIXME: puts("Evt Device Started: Setup\n\r");
                        if (ACI_STATUS_TRANSACTION_COMPLETE != do_aci_setup(&aci_state))
                        {
                            //FIXME: puts("Error in ACI Setup\n\r");
                        }
                        break;          
                    case ACI_DEVICE_STANDBY:
                        //FIXME: puts("Evt Device Started: Standby\n\r");
                        //Looking for an iPhone by sending radio advertisements
                        //When an iPhone connects to us we will get an ACI_EVT_CONNECTED event from the nRF8001
                        lib_aci_connect(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
                        //FIXME: puts("Advertising started\n\r");

                        delay_ms(100);
                        if (init_ble_int)
                        {
                            init_ble_int = 0;
                            attach_ble_interrupt();
                            ble_interrupt_mode_true();
                        }
                    

                        break;
                }
                break; //ACI Device Started Event
        
            case ACI_EVT_CMD_RSP:
                //If an ACI command response event comes with an error -> stop
                if (ACI_STATUS_SUCCESS != aci_evt->params.cmd_rsp.cmd_status)
                {
                    cmd_status = aci_evt->params.cmd_rsp.cmd_status;
                    //ACI ReadDynamicData and ACI WriteDynamicData will have status codes of
                    //TRANSACTION_CONTINUE and TRANSACTION_COMPLETE
                    //all other ACI commands will have status code of ACI_STATUS_SCUCCESS for a successful command
                    //FIXME: printf("ACI Command %X\n\r", aci_evt->params.cmd_rsp.cmd_opcode);
                    //FIXME: puts("Evt Cmd respone: Error. BOARD is in an while(1); loop\n\r");
                    //while (1);
                    // IGNORE ERROR FOR NOW - FIXME - FIGURE OUT WHY
                    break;
                }
                if (ACI_CMD_GET_DEVICE_VERSION == aci_evt->params.cmd_rsp.cmd_opcode)
                {
                    //Store the version and configuration information of the nRF8001
                    // in the Hardware Revision String Characteristic
                    lib_aci_set_local_data(&aci_state, PIPE_DEVICE_INFORMATION_HARDWARE_REVISION_STRING_SET, 
                                           (uint8_t *)&(aci_evt->params.cmd_rsp.params.get_device_version),
                                           sizeof(aci_evt_cmd_rsp_params_get_device_version_t));

                }        
                break;
        
            case ACI_EVT_CONNECTED:
                is_connected = 1;
                //FIXME: puts("Evt Connected\n\r");
                aci_state.data_credit_available = aci_state.data_credit_total;

                /*Get the device version of the nRF8001 and store it in the Hardware Revision String*/
                lib_aci_device_version();
                break;
        
            case ACI_EVT_PIPE_STATUS:
                //FIXME: puts("Evt Pipe Status\n\r");
                if (lib_aci_is_pipe_available(&aci_state, PIPE_UART_OVER_BTLE_UART_TX_TX) && (false == timing_change_done))
                {
                    lib_aci_change_timing_GAP_PPCP(); // change the timing on the link as specified
                    // in the nRFgo studio -> nRF8001 conf. -> GAP. 
                    // Used to increase or decrease bandwidth
                    timing_change_done = true;
                }
                break;
        
            case ACI_EVT_TIMING:
                //FIXME: puts("Evt link connection interval changed\n\r");
                break;
        
            case ACI_EVT_DISCONNECTED:
                is_connected = 0;
                ack = 1;
                //FIXME: puts("Evt Disconnected/Advertising timed out\n\r");
                lib_aci_connect(180/* in seconds */, 0x0100 /* advertising interval 100ms*/);
                //FIXME: puts("Advertising started\n\r");       
                break;
        
            case ACI_EVT_DATA_RECEIVED:
                for(int i=0; i<aci_evt->len - 2; i++)
                {
                    if(rx_buffer_len == MAX_RX_BUFF) 
                    {	
                        break;
                    }
                    else
                    {
                        if(p_back == &rx_buff[MAX_RX_BUFF])
                        {
                            p_back = &rx_buff[0];
                        }
                        *p_back = aci_evt->params.data_received.rx_data.aci_data[i];							
                        rx_buffer_len++;
                        p_back++;
                    }
                }
                break;
   
            case ACI_EVT_DATA_CREDIT:
                aci_state.data_credit_available = aci_state.data_credit_available + aci_evt->params.data_credit.credit;
                //FIXME: puts("ACI_EVT_DATA_CREDIT     \n\r");
                //FIXME: printf("Data Credit available: %d\n\r", aci_state.data_credit_available);
                ack=1;
                break;
      
            case ACI_EVT_PIPE_ERROR:
                //See the appendix in the nRF8001 Product Specication for details on the error codes
                //FIXME: printf("ACI Evt Pipe Error: Pipe #:%d\n\r", aci_evt->params.pipe_error.pipe_number);
                //FIXME: printf("  Pipe Error Code: %x\n\r", aci_evt->params.pipe_error.error_code);
                
                //Increment the credit available as the data packet was not sent
                aci_state.data_credit_available++;
                //FIXME: printf("Data Credit available: %d\n\r", aci_state.data_credit_available);
                break;         
        }
    }
    else
    {
        //puts("No ACI Events available");
        // No events in the ACI Event queue
        // Vector can go to sleep now
        // Wakeup from sleep from the RDYN line
        if (aci_state.aci_pins.interface_is_interrupt)//FIXMENOW && is_connected)
        {
            n_enter_sleep++;
            pmc_enable_sleepmode(0);
        }

    }
}

void ble_do_events()
{
    if (lib_aci_is_pipe_available(&aci_state, PIPE_UART_OVER_BTLE_UART_TX_TX))
    {
        unsigned char Index = 0;

        while(tx_buffer_len > 0)
        {
            int curr_bytes = tx_buffer_len > 20 ? 20 : tx_buffer_len;
                
            if(true == lib_aci_send_data(PIPE_UART_OVER_BTLE_UART_TX_TX, &tx_buff[Index], curr_bytes))
            {
                //FIXME: printf("data transmmit success!  Length: 20\n\r");
            }
            else
            {
                //FIXME: puts("data transmmit fail !\n\r");
            }
            
            tx_buffer_len -= curr_bytes;
            Index += curr_bytes;
            aci_state.data_credit_available--;
            //FIXME: printf("Data Credit available: %d\n\r", aci_state.data_credit_available);

            ack = 0;
            while (!ack)
                process_events();
        }

    }
    process_events();
}

