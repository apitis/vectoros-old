/*
 * i2c.c
 *
 * Created: 8/31/2013 03:29:31
 *  Author: alexr
 */ 

#include "i2c.h"

uint32_t i2c_write_byte(Twi *p_twi, uint8_t slave_chip, uint32_t reg_index, uint8_t  reg_value)
{
	twi_packet_t tx = {
	.addr        = { reg_index },
	.addr_length = 1,
	.buffer      = &reg_value,
	.length      = 1,
	.chip        = slave_chip
};

return twi_master_write(p_twi, &tx);
}

uint32_t i2c_write(Twi *p_twi, uint8_t slave_chip, uint32_t reg_index, uint16_t  reg_value)
{
	twi_packet_t tx = {
	.addr        = { reg_index },
	.addr_length = 1,
	.buffer      = &reg_value,
	.length      = 2,
	.chip        = slave_chip
	};

	return twi_master_write(p_twi, &tx);
}

uint32_t i2c_read(Twi *p_twi, uint8_t slave_chip, uint32_t  reg_index,	uint8_t  *reg_value)
{
		twi_packet_t rx = {
		.addr        = { reg_index },
		.addr_length = 1,
		.buffer      = reg_value,
		.length      = 1,
		.chip        = slave_chip
	};

	return twi_master_read(p_twi, &rx);
}

