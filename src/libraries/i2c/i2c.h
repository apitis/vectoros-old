/*
 * i2c.h
 *
 * Created: 8/31/2013 03:29:17
 *  Author: alexr
 */ 


#ifndef I2C_H_
#define I2C_H_

#include "asf.h"

uint32_t i2c_write_byte(Twi *p_twi, uint8_t slave_chip, uint32_t reg_index, uint8_t  reg_value);
uint32_t i2c_write(Twi *p_twi, uint8_t slave_chip, uint32_t reg_index, uint16_t  reg_value);
uint32_t i2c_read(Twi *p_twi, uint8_t slave_chip, uint32_t  reg_index,	uint8_t  *reg_value);


#endif /* I2C_H_ */