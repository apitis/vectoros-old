#include "asf.h"
#include "conf_board.h"

int bibi;

void flash_program(uint32_t address32, int offset, const unsigned char *buffer, unsigned length)
{
    uint32_t ul_page_addr = address32;
    uint32_t *pul_page = (uint32_t *) ul_page_addr;
    uint32_t ul_rc;
    uint32_t ul_idx;
    uint8_t uc_key;
    unsigned char ul_page_buffer[IFLASH_PAGE_SIZE];

    if (length > IFLASH_PAGE_SIZE)
        return;
    
    /* Initialize flash: 6 wait states for flash writing. */
    ul_rc = flash_init(FLASH_ACCESS_MODE_128, 6);
    if (ul_rc != FLASH_RC_OK) {
        bibi = 1;
        return;
    }

    /* Unlock page */
    ul_rc = flash_unlock(ul_page_addr,
                         ul_page_addr + IFLASH_PAGE_SIZE - 1, 0, 0);
    if (ul_rc != FLASH_RC_OK) {
        bibi = 2;
        return;
    }

    /* Write page */
    for (ul_idx = 0; ul_idx < length; ul_idx++) {
        ul_page_buffer[ul_idx] = buffer[ul_idx];
    }

    /* The EWP command is not supported for non-8KByte sectors in all devices
     *  SAM4 series, so an erase command is requried before the write operation.
     */
    ul_rc = flash_erase_page(ul_page_addr, IFLASH_ERASE_PAGES_8);
    if (ul_rc != FLASH_RC_OK) {
        bibi = 3;
        return;
    }

    ul_rc = flash_write(ul_page_addr, ul_page_buffer,
			IFLASH_PAGE_SIZE, 0);

    if (ul_rc != FLASH_RC_OK) {
        bibi = 4;
        return;
    }

    /* Validate page */
    for (ul_idx = 0; ul_idx < (IFLASH_PAGE_SIZE / 4); ul_idx++) {
        if (pul_page[ul_idx] != ul_page_buffer[ul_idx]) {
            bibi = 5;
            return;
        }
    }

    bibi = 6;
}


#define EEPROM_SIZE 256

int EEPROM_offset = 0;
uint8_t EEPROM_buffer[EEPROM_SIZE];

uint8_t EEPROM_read(int offset)
{
    return ((uint8_t *)BOND_START_ADDRESS)[offset];
}

void EEPROM_write(int offset, uint8_t b)
{
    EEPROM_buffer[offset] = b;

    /* Commit changes when writing to the status byte.  */
    if (offset == 0)
        flash_program(BOND_START_ADDRESS, 0, EEPROM_buffer, sizeof(EEPROM_buffer));
}

