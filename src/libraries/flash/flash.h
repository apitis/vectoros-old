#ifndef _FLASH_H
#define _FLASH_H

uint8_t EEPROM_read(int offset);
void EEPROM_write(int offset, uint8_t b);

#endif
