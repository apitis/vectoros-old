#ifndef SHARP_SCREEN_H
#define SHARP_SCREEN_H

enum {
    HELV16,
    HELV32,
    THIN42,
    WEATHER,
};

void refresh_screen();
void screen_put_text_at(char *msg, int l, int c);
void screen_put_bigtext_at(char *msg, int l, int c, int font);
void init_lcd_screen();
void screen_put_char(unsigned char c);
void screen_put_char_at(unsigned char c, int x, int y);
void draw_grafic(int line, int height);
void add_grafic_pixel(int v);
void screen_logo();
void screen_delete_lines(int line, int height);

        
#endif

