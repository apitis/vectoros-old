#include "asf.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "pio.h"
#include "bluetooth.h"
#include "bfcfont.h"

extern unsigned char reverse[];
extern uint8_t BLE_SPI_lock;

int grafic[128];
int gr_index = 0;

int min_grafic = 4096;
int max_grafic = 0;

int gr_height = 0;

unsigned char font[128][8] = {
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0000 (nul)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0001
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0002
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0003
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0004
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0005
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0006
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0007
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0008
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0009
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000A
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000B
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000C
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000D
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000E
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000F
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0010
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0011
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0012
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0013
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0014
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0015
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0016
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0017
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0018
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0019
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001A
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001B
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001C
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001D
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001E
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001F
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0020 (space)
    { 0x18, 0x3C, 0x3C, 0x18, 0x18, 0x00, 0x18, 0x00},   // U+0021 (!)
    { 0x36, 0x36, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0022 (")
    { 0x36, 0x36, 0x7F, 0x36, 0x7F, 0x36, 0x36, 0x00},   // U+0023 (#)
    { 0x0C, 0x3E, 0x03, 0x1E, 0x30, 0x1F, 0x0C, 0x00},   // U+0024 ($)
    { 0x00, 0x63, 0x33, 0x18, 0x0C, 0x66, 0x63, 0x00},   // U+0025 (%)
    { 0x1C, 0x36, 0x1C, 0x6E, 0x3B, 0x33, 0x6E, 0x00},   // U+0026 (&)
    { 0x06, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0027 (')
    { 0x18, 0x0C, 0x06, 0x06, 0x06, 0x0C, 0x18, 0x00},   // U+0028 (()
    { 0x06, 0x0C, 0x18, 0x18, 0x18, 0x0C, 0x06, 0x00},   // U+0029 ())
    { 0x00, 0x66, 0x3C, 0xFF, 0x3C, 0x66, 0x00, 0x00},   // U+002A (*)
    { 0x00, 0x0C, 0x0C, 0x3F, 0x0C, 0x0C, 0x00, 0x00},   // U+002B (+)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x0C, 0x06},   // U+002C (,)
    { 0x00, 0x00, 0x00, 0x3F, 0x00, 0x00, 0x00, 0x00},   // U+002D (-)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x0C, 0x00},   // U+002E (.)
    { 0x60, 0x30, 0x18, 0x0C, 0x06, 0x03, 0x01, 0x00},   // U+002F (/)
    { 0x3E, 0x63, 0x73, 0x7B, 0x6F, 0x67, 0x3E, 0x00},   // U+0030 (0)
    { 0x0C, 0x0E, 0x0C, 0x0C, 0x0C, 0x0C, 0x3F, 0x00},   // U+0031 (1)
    { 0x1E, 0x33, 0x30, 0x1C, 0x06, 0x33, 0x3F, 0x00},   // U+0032 (2)
    { 0x1E, 0x33, 0x30, 0x1C, 0x30, 0x33, 0x1E, 0x00},   // U+0033 (3)
    { 0x38, 0x3C, 0x36, 0x33, 0x7F, 0x30, 0x78, 0x00},   // U+0034 (4)
    { 0x3F, 0x03, 0x1F, 0x30, 0x30, 0x33, 0x1E, 0x00},   // U+0035 (5)
    { 0x1C, 0x06, 0x03, 0x1F, 0x33, 0x33, 0x1E, 0x00},   // U+0036 (6)
    { 0x3F, 0x33, 0x30, 0x18, 0x0C, 0x0C, 0x0C, 0x00},   // U+0037 (7)
    { 0x1E, 0x33, 0x33, 0x1E, 0x33, 0x33, 0x1E, 0x00},   // U+0038 (8)
    { 0x1E, 0x33, 0x33, 0x3E, 0x30, 0x18, 0x0E, 0x00},   // U+0039 (9)
    { 0x00, 0x0C, 0x0C, 0x00, 0x00, 0x0C, 0x0C, 0x00},   // U+003A (:)
    { 0x00, 0x0C, 0x0C, 0x00, 0x00, 0x0C, 0x0C, 0x06},   // U+003B (//)
    { 0x18, 0x0C, 0x06, 0x03, 0x06, 0x0C, 0x18, 0x00},   // U+003C (<)
    { 0x00, 0x00, 0x3F, 0x00, 0x00, 0x3F, 0x00, 0x00},   // U+003D (=)
    { 0x06, 0x0C, 0x18, 0x30, 0x18, 0x0C, 0x06, 0x00},   // U+003E (>)
    { 0x1E, 0x33, 0x30, 0x18, 0x0C, 0x00, 0x0C, 0x00},   // U+003F (?)
    { 0x3E, 0x63, 0x7B, 0x7B, 0x7B, 0x03, 0x1E, 0x00},   // U+0040 (@)
    { 0x0C, 0x1E, 0x33, 0x33, 0x3F, 0x33, 0x33, 0x00},   // U+0041 (A)
    { 0x3F, 0x66, 0x66, 0x3E, 0x66, 0x66, 0x3F, 0x00},   // U+0042 (B)
    { 0x3C, 0x66, 0x03, 0x03, 0x03, 0x66, 0x3C, 0x00},   // U+0043 (C)
    { 0x1F, 0x36, 0x66, 0x66, 0x66, 0x36, 0x1F, 0x00},   // U+0044 (D)
    { 0x7F, 0x46, 0x16, 0x1E, 0x16, 0x46, 0x7F, 0x00},   // U+0045 (E)
    { 0x7F, 0x46, 0x16, 0x1E, 0x16, 0x06, 0x0F, 0x00},   // U+0046 (F)
    { 0x3C, 0x66, 0x03, 0x03, 0x73, 0x66, 0x7C, 0x00},   // U+0047 (G)
    { 0x33, 0x33, 0x33, 0x3F, 0x33, 0x33, 0x33, 0x00},   // U+0048 (H)
    { 0x1E, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+0049 (I)
    { 0x78, 0x30, 0x30, 0x30, 0x33, 0x33, 0x1E, 0x00},   // U+004A (J)
    { 0x67, 0x66, 0x36, 0x1E, 0x36, 0x66, 0x67, 0x00},   // U+004B (K)
    { 0x0F, 0x06, 0x06, 0x06, 0x46, 0x66, 0x7F, 0x00},   // U+004C (L)
    { 0x63, 0x77, 0x7F, 0x7F, 0x6B, 0x63, 0x63, 0x00},   // U+004D (M)
    { 0x63, 0x67, 0x6F, 0x7B, 0x73, 0x63, 0x63, 0x00},   // U+004E (N)
    { 0x1C, 0x36, 0x63, 0x63, 0x63, 0x36, 0x1C, 0x00},   // U+004F (O)
    { 0x3F, 0x66, 0x66, 0x3E, 0x06, 0x06, 0x0F, 0x00},   // U+0050 (P)
    { 0x1E, 0x33, 0x33, 0x33, 0x3B, 0x1E, 0x38, 0x00},   // U+0051 (Q)
    { 0x3F, 0x66, 0x66, 0x3E, 0x36, 0x66, 0x67, 0x00},   // U+0052 (R)
    { 0x1E, 0x33, 0x07, 0x0E, 0x38, 0x33, 0x1E, 0x00},   // U+0053 (S)
    { 0x3F, 0x2D, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+0054 (T)
    { 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x3F, 0x00},   // U+0055 (U)
    { 0x33, 0x33, 0x33, 0x33, 0x33, 0x1E, 0x0C, 0x00},   // U+0056 (V)
    { 0x63, 0x63, 0x63, 0x6B, 0x7F, 0x77, 0x63, 0x00},   // U+0057 (W)
    { 0x63, 0x63, 0x36, 0x1C, 0x1C, 0x36, 0x63, 0x00},   // U+0058 (X)
    { 0x33, 0x33, 0x33, 0x1E, 0x0C, 0x0C, 0x1E, 0x00},   // U+0059 (Y)
    { 0x7F, 0x63, 0x31, 0x18, 0x4C, 0x66, 0x7F, 0x00},   // U+005A (Z)
    { 0x1E, 0x06, 0x06, 0x06, 0x06, 0x06, 0x1E, 0x00},   // U+005B ([)
    { 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x40, 0x00},   // U+005C (\)
    { 0x1E, 0x18, 0x18, 0x18, 0x18, 0x18, 0x1E, 0x00},   // U+005D (])
    { 0x08, 0x1C, 0x36, 0x63, 0x00, 0x00, 0x00, 0x00},   // U+005E (^)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF},   // U+005F (_)
    { 0x0C, 0x0C, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0060 (`)
    { 0x00, 0x00, 0x1E, 0x30, 0x3E, 0x33, 0x6E, 0x00},   // U+0061 (a)
    { 0x07, 0x06, 0x06, 0x3E, 0x66, 0x66, 0x3B, 0x00},   // U+0062 (b)
    { 0x00, 0x00, 0x1E, 0x33, 0x03, 0x33, 0x1E, 0x00},   // U+0063 (c)
    { 0x38, 0x30, 0x30, 0x3e, 0x33, 0x33, 0x6E, 0x00},   // U+0064 (d)
    { 0x00, 0x00, 0x1E, 0x33, 0x3f, 0x03, 0x1E, 0x00},   // U+0065 (e)
    { 0x1C, 0x36, 0x06, 0x0f, 0x06, 0x06, 0x0F, 0x00},   // U+0066 (f)
    { 0x00, 0x00, 0x6E, 0x33, 0x33, 0x3E, 0x30, 0x1F},   // U+0067 (g)
    { 0x07, 0x06, 0x36, 0x6E, 0x66, 0x66, 0x67, 0x00},   // U+0068 (h)
    { 0x0C, 0x00, 0x0E, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+0069 (i)
    { 0x30, 0x00, 0x30, 0x30, 0x30, 0x33, 0x33, 0x1E},   // U+006A (j)
    { 0x07, 0x06, 0x66, 0x36, 0x1E, 0x36, 0x67, 0x00},   // U+006B (k)
    { 0x0E, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+006C (l)
    { 0x00, 0x00, 0x33, 0x7F, 0x7F, 0x6B, 0x63, 0x00},   // U+006D (m)
    { 0x00, 0x00, 0x1F, 0x33, 0x33, 0x33, 0x33, 0x00},   // U+006E (n)
    { 0x00, 0x00, 0x1E, 0x33, 0x33, 0x33, 0x1E, 0x00},   // U+006F (o)
    { 0x00, 0x00, 0x3B, 0x66, 0x66, 0x3E, 0x06, 0x0F},   // U+0070 (p)
    { 0x00, 0x00, 0x6E, 0x33, 0x33, 0x3E, 0x30, 0x78},   // U+0071 (q)
    { 0x00, 0x00, 0x3B, 0x6E, 0x66, 0x06, 0x0F, 0x00},   // U+0072 (r)
    { 0x00, 0x00, 0x3E, 0x03, 0x1E, 0x30, 0x1F, 0x00},   // U+0073 (s)
    { 0x08, 0x0C, 0x3E, 0x0C, 0x0C, 0x2C, 0x18, 0x00},   // U+0074 (t)
    { 0x00, 0x00, 0x33, 0x33, 0x33, 0x33, 0x6E, 0x00},   // U+0075 (u)
    { 0x00, 0x00, 0x33, 0x33, 0x33, 0x1E, 0x0C, 0x00},   // U+0076 (v)
    { 0x00, 0x00, 0x63, 0x6B, 0x7F, 0x7F, 0x36, 0x00},   // U+0077 (w)
    { 0x00, 0x00, 0x63, 0x36, 0x1C, 0x36, 0x63, 0x00},   // U+0078 (x)
    { 0x00, 0x00, 0x33, 0x33, 0x33, 0x3E, 0x30, 0x1F},   // U+0079 (y)
    { 0x00, 0x00, 0x3F, 0x19, 0x0C, 0x26, 0x3F, 0x00},   // U+007A (z)
    { 0x38, 0x0C, 0x0C, 0x07, 0x0C, 0x0C, 0x38, 0x00},   // U+007B ({)
    { 0x18, 0x18, 0x18, 0x00, 0x18, 0x18, 0x18, 0x00},   // U+007C (|)
    { 0x07, 0x0C, 0x0C, 0x38, 0x0C, 0x0C, 0x07, 0x00},   // U+007D (})
    { 0x6E, 0x3B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+007E (~)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // U+007F
};

uint8_t screen[128][16];
uint8_t line_dirty[128];

extern int ble_interrupt_attached;

void refresh_screen()
{
    int i, j;
    static uint8_t VCOM = 0;

    uint16_t data = 0;
    uint8_t uc_pcs = 0;

    uint8_t first_line = 1;
    uint8_t disabled_ble = 0;

    /* Wait for a pending BLE event to complete.
       For now avoid refreshing, in the future, reschedule the update.  */
    if (BLE_SPI_lock)
        return;

    pmc_switch_mck_to_mainck(SYSCLK_PRES_1);
    
    /* Cannot speak with BT while updating the screen.  */
    if (ble_interrupt_attached)
    {
        pio_disable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
        disabled_ble = 1;
    }

    /* Select display.  */
    pio_set_pin_high(SHARP_SCS);

    /* Toggle V_COM bit.  */
    VCOM ^= 0x40;
        
    for (i = 0; i < 128; i++)
    {
        /* Update only dirty lines.  */
        if (line_dirty[i])
        {
            /* Issue line update command at the beggining of the first line.  */
            if (first_line)
            {
                first_line = 0;

                spi_write(SPI_MASTER_BASE, 0x80 | VCOM, 0, 0);
                spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
            }

            /* Line clean :).  */
            line_dirty[i] = 0;
            
            spi_write(SPI_MASTER_BASE, reverse[i + 1], 0, 0);
            spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
            
            for (j = 0; j < 16; j++)
            {
                spi_write(SPI_MASTER_BASE, screen[i][j], 0, 0);
                spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
            }
            
            spi_write(SPI_MASTER_BASE, 0, 0, 0);
            spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
        }
    }

    /* If there was no dirty line, issue V_COM update command.  */
    if (first_line)
    {
        spi_write(SPI_MASTER_BASE, 0x00 | VCOM, 0, 0);
        spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
    }
    
    spi_write(SPI_MASTER_BASE, 0, 0, 0);
    spi_read(SPI_MASTER_BASE, &data, &uc_pcs);

    /* Deselect display.  */
    pio_set_pin_low(SHARP_SCS);

    /* Speak with BT again.  */
    if (disabled_ble)
        pio_enable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
}

int line = 0;
int col = 0;

/* extern const unsigned char fontArial24h_data_table[]; */
/* extern const unsigned char fontArial24h_index_table[]; */
/* extern const unsigned fontArial24h_offset_table[]; */
/* extern const unsigned char fontArial24h_width_table[]; */


const unsigned int a_maskR[9] = {
    0, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0xFF,
};
    
const unsigned int a_maskL[9] = {
    0, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF,
};
    
/* extern const BFC_FONT font16hHelveticaBold; */
/* extern const BFC_FONT font24hHelvetica; */
/* extern const BFC_FONT font24hHelveticaBold; */
/* extern const BFC_FONT font32hHelveticaBold; */
/* extern const BFC_FONT font34hHelvetica; */
/* extern const BFC_FONT fontTahoma36h; */
/* extern const BFC_FONT fontColaborate_Medium42h; */

extern const BFC_FONT font16hHelvetica;
extern const BFC_FONT font32hHelvetica;
extern const BFC_FONT fontColaborate_Thin42h;
extern const BFC_FONT fontWeatherImages24h;

const BFC_FONT *Fonts[16] = {
    &font16hHelvetica,
    &font32hHelvetica,
    &fontColaborate_Thin42h,
    &fontWeatherImages24h,
};

int char_width(unsigned char c, int font_nr)
{
    const BFC_FONT *font = Fonts[font_nr];
    int char_index = c - font->p.pProp->FirstChar;
    const BFC_CHARINFO *pchar = &(font->p.pProp->pFirstCharInfo[char_index]);

    return pchar->Width;
}

int screen_put_char_at(unsigned char c, int x, int y, int font_nr)
{
    const BFC_FONT *font = Fonts[font_nr];
    int char_index = c - font->p.pProp->FirstChar;
    const BFC_CHARINFO *pchar = &(font->p.pProp->pFirstCharInfo[char_index]);
    int width = pchar->Width;
    int height = font->FontHeight;
    
    int offset = 0;


    for (int l = y; l < y + height; l++)
    {
        unsigned char src_bits = ~pchar->p.pData8[offset++];
        unsigned char *p_dst_bits = (unsigned char *)screen + l * 16 + (x >> 3);

        unsigned char src_mask = 1 << 7;
        unsigned char dst_mask = 1 << (7 - (x & 0x07));
        
        for (int i = 0; i < width; i++)
        {
            if (!dst_mask)
            {
                p_dst_bits++;
                dst_mask = 0x80;
            }

            if (!src_mask)
            {
                src_bits = ~pchar->p.pData8[offset++];
                src_mask = 0x80;
            }

            if (src_bits & src_mask)
            {
                /* Have to set the bit to 1.  */
                if (!(*p_dst_bits & dst_mask))
                {
                    *p_dst_bits |= dst_mask;
                    line_dirty[l] = 1;
                }
            }
            else
            {
                /* Have to set the bit to 0.  */
                if (*p_dst_bits & dst_mask)
                {
                    *p_dst_bits &= ~dst_mask;
                    line_dirty[l] = 1;
                }
            }
            
            dst_mask >>= 1;
            src_mask >>= 1;
        }
    }
    
    return width;
}

void screen_put_bigtext_at(char *msg, int x, int y, int font_nr)
{
    char *s = msg;
    int width = 0;
    
    /* -1 = center text
       -2 = right aligned
    */
    if (x < 0)
    {
        while (*s)
            width += char_width(*s++, font_nr);

        if (x == -1)
            x = (128 - width) >> 1;
        else if (x == -2)
            x = 128 - width - 1;
    }

    for (s = msg; *s; s++)
    {
        /* Clip text.  */
        if (x + char_width(*s, font_nr) >= 128)
            return;
        
        x += screen_put_char_at(*s, x, y, font_nr);
    }
}


void screen_put_char(unsigned char c)
{
    if (c == 0x0A)
        line++;
    else
        if (c == 0x0D)
            col = 0;
        else
        {
            for (int i = 0; i < 8; i++)
            {
                int l_line = line * 8 + i;

                char b = reverse[(unsigned)font[(unsigned)c][i]] ^ 0xFF;
                
                if (screen[l_line][col] != b)
                {
                    screen[l_line][col] = b;
                    line_dirty[l_line] = 1;
                }
                
            }
            
            col++;

            if (col >= 16)
            {
                col = 0;
                line++;
            }
        }
        
    if (line >= 16)
        line = 0;
}

void screen_put_text(char *msg)
{
    char *s = msg;

    while (*s)
    {
        screen_put_char(*s++);
    }
}

void screen_put_text_at(char *msg, int l, int c)
{
    line = l;
    col = c;

    screen_put_text(msg);
}

#define vvvv_width 128
#define vvvv_height 48
static char vvvv_bits[] = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 
  0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x0C, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x0E, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x00, 
  0x80, 0x67, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0xE6, 0x01, 0xC0, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xCC, 0x07, 0xF0, 0x19, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0x1F, 
  0xFC, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x38, 0x7F, 0x7F, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x7E, 0x3F, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x7C, 
  0x8F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0xE0, 0x71, 0xC7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x63, 0xE3, 0x01, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x47, 
  0xF1, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x80, 0x0F, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0x7E, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 
  0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x7E, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7C, 0x1F, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7C, 
  0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x78, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x07, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x60, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x01, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x07, 0x00, 0x18, 0xF0, 0x7F, 0x00, 0xFE, 0x3F, 0xFF, 0xFF, 0x1F, 0xE0, 
  0x7F, 0x00, 0xF0, 0xFF, 0x06, 0x00, 0x0C, 0xFE, 0xFF, 0x81, 0xFF, 0x3F, 
  0xFF, 0xFF, 0x1F, 0xF8, 0xFF, 0x03, 0xFC, 0xFF, 0x0C, 0x00, 0x0E, 0x1F, 
  0xC0, 0xE3, 0x07, 0x00, 0x30, 0x00, 0x00, 0x3E, 0xC0, 0x07, 0x1E, 0x00, 
  0x1C, 0x00, 0x86, 0x03, 0x00, 0x63, 0x00, 0x00, 0x30, 0x00, 0x00, 0x06, 
  0x00, 0x0E, 0x06, 0x00, 0x18, 0x00, 0x87, 0x01, 0x00, 0x73, 0x00, 0x00, 
  0x30, 0x00, 0x00, 0x07, 0x00, 0x0C, 0x07, 0x00, 0x38, 0x80, 0x83, 0x01, 
  0x00, 0x33, 0x00, 0x00, 0x30, 0x00, 0x00, 0x03, 0x00, 0x0C, 0x07, 0x00, 
  0x70, 0x80, 0x81, 0x01, 0x80, 0x33, 0x00, 0x00, 0x30, 0x00, 0x00, 0x03, 
  0x00, 0x0C, 0x07, 0x00, 0x60, 0xC0, 0x81, 0xFF, 0xFF, 0x31, 0x00, 0x00, 
  0x30, 0x00, 0x00, 0x03, 0x00, 0x1C, 0x07, 0x00, 0xE0, 0xE0, 0x80, 0xFF, 
  0xFF, 0x30, 0x00, 0x00, 0x30, 0x00, 0x00, 0x03, 0x00, 0x0C, 0x07, 0x00, 
  0xC0, 0x61, 0x80, 0x01, 0x00, 0x30, 0x00, 0x00, 0x30, 0x00, 0x00, 0x03, 
  0x00, 0x0C, 0x07, 0x00, 0x80, 0x71, 0x80, 0x01, 0x00, 0x70, 0x00, 0x00, 
  0x30, 0x00, 0x00, 0x07, 0x00, 0x0C, 0x07, 0x00, 0x80, 0x3B, 0x80, 0x03, 
  0x00, 0x60, 0x00, 0x00, 0x70, 0x00, 0x00, 0x06, 0x00, 0x0E, 0x07, 0x00, 
  0x00, 0x1F, 0x00, 0x0F, 0x00, 0xE0, 0x01, 0x00, 0xE0, 0x00, 0x00, 0x1E, 
  0x00, 0x07, 0x07, 0x00, 0x00, 0x1E, 0x00, 0xFE, 0xFF, 0xC1, 0xFF, 0x3F, 
  0xE0, 0xFF, 0x1F, 0xFC, 0xFF, 0x03, 0x07, 0x00, 0x00, 0x0E, 0x00, 0xF8, 
  0xFF, 0x01, 0xFF, 0x3F, 0x80, 0xFF, 0x1F, 0xF0, 0xFF, 0x00, 0x07, 0x00, 
  };



void screen_put_image(int line, char *v, int w, int h, int start_line)
{
    int l0 = line;
    
    int i = 0, j = 0;

    for (i = l0; i < l0 + h; i++)
    {
        int ld = 0;
        
        for (j = 0; j < 16; j++)
        {
            char b = reverse[v[(i + start_line - l0) * 16 + j] ^ 0xFF];
            if (screen[i][j] != b)
            {
                screen[i][j] = b;
                ld = 1;
            }
        }

        line_dirty[i] = ld;
    }
}

void init_lcd_screen()
{
    int i, j;
    uint16_t data = 0;
    uint8_t uc_pcs = 0;

    
    pio_configure_pin(SHARP_SCS, PIO_TYPE_PIO_OUTPUT_1);

    for (i = 0; i < 128; i++)
    {
        for (j = 0; j < 16; j++)
            screen[i][j] = 0xFF;

        line_dirty[i] = 1;

        grafic[i] = 0;
    }

    
    pio_set_pin_low(SHARP_SCS);

    pio_disable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
    pio_set_pin_high(SHARP_SCS);
    spi_write(SPI_MASTER_BASE, 0x60, 0, 0);
    spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
    spi_write(SPI_MASTER_BASE, 0, 0, 0);
    spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
    pio_set_pin_low(SHARP_SCS);
    pio_enable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
    
}

void screen_logo()
{
    screen_put_image(90, vvvv_bits, 128, 23, 25);
}

void screen_delete_lines(int line, int height)
{
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < 16; j ++)
            if (screen[i + line][j] != 0xFF)
            {
                screen[i + line][j] = 0xFF;
                
                line_dirty[i + line] = 1;
            }
    }
}

void add_grafic_pixel(int v)
{
    if (gr_index > 0)
    {
        int modul = grafic[gr_index - 1] - v;
        if (modul > 0)
        {
            if (modul > 16)
                v = grafic[gr_index - 1] - 16;
        }
        else
        {
            if (modul < -16)
                v = grafic[gr_index - 1] + 16;
        }
    }
        
    grafic[gr_index++] = v;

    if (v < min_grafic)
        min_grafic = v;

    if (v > max_grafic)
        max_grafic = v;

    if (max_grafic - min_grafic < 40)
        max_grafic = min_grafic + 40;
    
    gr_height = max_grafic - min_grafic;
    
    if (gr_index >= 128)
    {
        min_grafic = 4096;
        max_grafic = 0;
        gr_index = 0;
        gr_height = 0;
    }
}

void put_pixel(int x, int y1, int y2)
{
    for (int y = y1; y < y2; y++)
        screen[y][x >> 3] &= ~(1 << (7 - (x & 7)));
}

void draw_grafic(int l, int h)
{
    for (int i = l; i < l + h; i++)
    {
        for (int j = 0; j < 16; j++)
            screen[i][j] = 0xFF;
        
        line_dirty[i] = 1;
    }

    if (max_grafic == min_grafic)
        max_grafic = min_grafic + 1;
    
    if (max_grafic > min_grafic)
        for (int i = 0; i < gr_index; i++)
        {
            int y1 = (grafic[i] - min_grafic) * h / (max_grafic - min_grafic);
            int y2 = (i == 0) ? y1 : (grafic[i-1] - min_grafic) * h / (max_grafic - min_grafic);

            put_pixel(i, (y1 < y2 ? y1 : y2) + l, (y1 < y2 ? y2 : y1) + l);
        }
    
}

