
#include <SPI.h>
#include <lib_aci.h>

#include <aci_setup.h>
#include <EEPROM.h>
#include "pack_lib.h"
#include "ancs_base.h"
#include "vector.h"

/**
Put the nRF8001 setup in the RAM of the nRF8001.
*/
#include "services.h"
/**
Include the services_lock.h to put the setup in the OTP memory of the nRF8001.
This would mean that the setup cannot be changed once put in.
However this removes the need to do the setup of the nRF8001 on every reset.
*/


#ifdef SERVICES_PIPE_TYPE_MAPPING_CONTENT
  static services_pipe_type_mapping_t
      services_pipe_type_mapping[NUMBER_OF_PIPES] = SERVICES_PIPE_TYPE_MAPPING_CONTENT;
#else
  #define NUMBER_OF_PIPES 0
  static services_pipe_type_mapping_t * services_pipe_type_mapping = NULL;
#endif

/* Store the setup for the nRF8001 in the flash of the AVR to save on RAM */
static hal_aci_data_t setup_msgs[NB_SETUP_MESSAGES] PROGMEM = SETUP_MESSAGES_CONTENT;

// aci_struct that will contain
// total initial credits
// current credit
// current state of the aci (setup/standby/active/sleep)
// open remote pipe pending
// close remote pipe pending
// Current pipe available bitmap
// Current pipe closed bitmap
// Current connection interval, slave latency and link supervision timeout
// Current State of the the GATT client (Service Discovery)
// Status of the bond (R) Peer address
static struct aci_state_t aci_state;

/*
Temporary buffers for sending ACI commands
*/
static hal_aci_evt_t  aci_data;
static hal_aci_data_t aci_cmd;

static uint8_t         uart_buffer[20];
static uint8_t         uart_buffer_len = 0;
static uint8_t         dummychar = 0;


/*
We will store the bonding info for the nRF8001 in the EEPROM/Flash of the MCU to recover from a power loss situation
*/
static bool bonded_first_time = true;

/*
Timing change state variable
*/
static bool timing_change_done = false;


/* Define how assert should function in the BLE library */
void __ble_assert(const char *file, uint16_t line)
{
  Serial.print("ERROR ");
  while(1);
}

/*************NOTE**********
Scroll to the end of the file and read the loop() and setup() functions.
The loop/setup functions is the equivalent of the main() function
*/

/*
Read the Dymamic data from the EEPROM and send then as ACI Write Dynamic Data to the nRF8001
This will restore the nRF8001 to the situation when the Dynamic Data was Read out
*/
aci_status_code_t bond_data_restore(aci_state_t *aci_stat, uint8_t eeprom_status, bool *bonded_first_time_state)
{
  aci_evt_t *aci_evt;
  uint8_t eeprom_offset_read = 1;
  uint8_t write_dyn_num_msgs = 0;
  uint8_t len =0;

  // Get the number of messages to write for the eeprom_status
  write_dyn_num_msgs = eeprom_status & 0x7F;
  //Read from the EEPROM
  while(1)
  {
    len = EEPROM.read(eeprom_offset_read);
    eeprom_offset_read++;
    aci_cmd.buffer[0] = len;

    for (uint8_t i=1; i<=len; i++)
    {
        aci_cmd.buffer[i] = EEPROM.read(eeprom_offset_read);
        eeprom_offset_read++;
    }
    //Send the ACI Write Dynamic Data
    if (!hal_aci_tl_send(&aci_cmd))
    {
      return ACI_STATUS_ERROR_INTERNAL;
    }

    //Spin in the while loop waiting for an event
    while (1)
    {
      if (lib_aci_event_get(aci_stat, &aci_data))
      {
        aci_evt = &aci_data.evt;

        if (ACI_EVT_CMD_RSP != aci_evt->evt_opcode)
        {
          //Got something other than a command response evt -> Error
          return ACI_STATUS_ERROR_INTERNAL;
        }
        else
        {
          write_dyn_num_msgs--;

          //ACI Evt Command Response
          if (ACI_STATUS_TRANSACTION_COMPLETE == aci_evt->params.cmd_rsp.cmd_status)
          {
            //Set the state variables correctly
            *bonded_first_time_state = false;
            aci_stat->bonded = ACI_BOND_STATUS_SUCCESS;
            delay(10);

            return ACI_STATUS_TRANSACTION_COMPLETE;
          }
          if (0 >= write_dyn_num_msgs)
          {
            //should have returned earlier
            return ACI_STATUS_ERROR_INTERNAL;
          }
          if (ACI_STATUS_TRANSACTION_CONTINUE == aci_evt->params.cmd_rsp.cmd_status)
          {
            //break and write the next ACI Write Dynamic Data
            break;
          }
        }
      }
    }
  }
}


/*
This function is specific to the atmega328
@params ACI Command Response Evt received from the Read Dynmaic Data
*/
void bond_data_store(aci_evt_t *evt)
{
  static int eeprom_write_offset = 1;

  //Write it to non-volatile storage
  EEPROM.write( eeprom_write_offset, evt->len -2 );
  eeprom_write_offset++;

  EEPROM.write( eeprom_write_offset, ACI_CMD_WRITE_DYNAMIC_DATA);
  eeprom_write_offset++;

  for (uint8_t i=0; i< (evt->len-3); i++)
  {
    EEPROM.write( eeprom_write_offset, evt->params.cmd_rsp.params.padding[i]);
    eeprom_write_offset++;
  }
}

bool once = false;

bool bond_data_read_store(aci_state_t *aci_stat)
{
  /*
  The size of the dynamic data for a specific Bluetooth Low Energy configuration
  is present in the ublue_setup.gen.out.txt generated by the nRFgo studio as "dynamic data size".
  */
  bool status = false;
  aci_evt_t * aci_evt = NULL;
  uint8_t read_dyn_num_msgs = 0;

  //Start reading the dynamic data
  lib_aci_read_dynamic_data();
  read_dyn_num_msgs++;

  while (1)
  {
    if (true == lib_aci_event_get(aci_stat, &aci_data))
    {
      aci_evt = &aci_data.evt;

      if (ACI_EVT_CMD_RSP != aci_evt->evt_opcode )
      {
        //Got something other than a command response evt -> Error
        status = false;
        break;
      }

      if (ACI_STATUS_TRANSACTION_COMPLETE == aci_evt->params.cmd_rsp.cmd_status)
      {
        //Store the contents of the command response event in the EEPROM
        //(len, cmd, seq-no, data) : cmd ->Write Dynamic Data so it can be used directly
        bond_data_store(aci_evt);

        //Set the flag in the EEPROM that the contents of the EEPROM is valid
        EEPROM.write(0, 0x80|read_dyn_num_msgs );
        //Finished with reading the dynamic data
        status = true;

        break;
      }

      if (!(ACI_STATUS_TRANSACTION_CONTINUE == aci_evt->params.cmd_rsp.cmd_status))
      {
        //We failed the read dymanic data
        //Set the flag in the EEPROM that the contents of the EEPROM is invalid
        EEPROM.write(0, 0x00);

        status = false;
        break;
      }
      else
      {
        //Store the contents of the command response event in the EEPROM
        // (len, cmd, seq-no, data) : cmd ->Write Dynamic Data so it can be used directly when re-storing the dynamic data
        bond_data_store(aci_evt);

        //Read the next dynamic data message
        lib_aci_read_dynamic_data();
        read_dyn_num_msgs++;
      }
    }
  }
  return status;
}


bool once_not = true;
bool once_data = true;
bool once_control = true;

void aci_loop()
{
  static bool setup_required = false;

  // We enter the if statement only when there is a ACI event available to be processed
  if (lib_aci_event_get(&aci_state, &aci_data))
  {
    aci_evt_t * aci_evt;

    aci_evt = &aci_data.evt;
    switch(aci_evt->evt_opcode)
    {
      /**
      As soon as you reset the nRF8001 you will get an ACI Device Started Event
      */
      case ACI_EVT_DEVICE_STARTED:
      {
        aci_state.data_credit_total = aci_evt->params.device_started.credit_available;
        switch(aci_evt->params.device_started.device_mode)
        {
          case ACI_DEVICE_SETUP:
            /**
            When the device is in the setup mode
            */
            Serial.println(F("Evt Device Started: Setup"));
            setup_required = true;
            break;

          case ACI_DEVICE_STANDBY:
          
            
            Serial.println(F("Evt Device Started: Standby"));
            if (aci_evt->params.device_started.hw_error)
            {
              delay(20); //Magic number used to make sure the HW error event is handled correctly.
            }
            else
            {
              //Manage the bond in EEPROM of the AVR
              {
                uint8_t eeprom_status = 0;
                eeprom_status = EEPROM.read(0);
                if (eeprom_status != 0x00)
                {
                  Serial.println(F("Previous Bond present. Restoring"));
                  //We must have lost power and restarted and must restore the bonding infromation using the ACI Write Dynamic Data

                  if (ACI_STATUS_TRANSACTION_COMPLETE == bond_data_restore(&aci_state, eeprom_status, &bonded_first_time))
                  {
                    Serial.println(F("Bond restored successfully"));
                  }
                  else
                  {
                    Serial.println(F("Bond restore failed. Delete the bond and try again."));
                  }
                }
              }

              // Start bonding as all proximity devices need to be bonded to be usable
              if (ACI_BOND_STATUS_SUCCESS != aci_state.bonded)
              {
//                char a[5] = "Ve";
//                a[2] = (char)(((int)'0')+random(9));
//                a[3] = (char)(((int)'0')+random(9));
//                a[4] = (char)(((int)'0')+random(9));                
//                bool check = lib_aci_set_local_data(&aci_state, PIPE_GAP_DEVICE_NAME_SET,(uint8_t *)a, 5);
//                if( check ) Serial.println("S-a schimbat numele!");
                
                lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
                Serial.println(F("No Bond present in EEPROM."));
              }
              else
              {
                //connect to an already bonded device
                //Use lib_aci_direct_connect for faster re-connections with PC, not recommended to use with iOS/OS X
                lib_aci_connect(100/* in seconds */, 0x0020 /* advertising interval 20ms*/);
                Serial.println(F("Already bonded : Advertising started : Waiting to be connected"));
              }
            }
            break;
        }
      }
        break; //ACI Device Started Event

      case ACI_EVT_CMD_RSP:
        //If an ACI command response event comes with an error -> stop
        if (ACI_STATUS_SUCCESS != aci_evt->params.cmd_rsp.cmd_status)
        {
          //ACI ReadDynamicData and ACI WriteDynamicData will have status codes of
          //TRANSACTION_CONTINUE and TRANSACTION_COMPLETE
          //all other ACI commands will have status code of ACI_STATUS_SCUCCESS for a successful command
          Serial.print(F("ACI Command "));
          Serial.println(aci_evt->params.cmd_rsp.cmd_opcode, HEX);
          Serial.print(F("Evt Cmd respone: Status "));
          Serial.println(aci_evt->params.cmd_rsp.cmd_status, HEX);
          if( ACI_STATUS_ERROR_CMD_UNKNOWN == aci_evt->params.cmd_rsp.cmd_status)
          {
            Serial.println("CMD UNKNOWN");
            //ar trebui un hardware reset aici
          }
        }

        if (ACI_CMD_GET_DEVICE_VERSION == aci_evt->params.cmd_rsp.cmd_opcode)
        {
          //Store the version and configuration information of the nRF8001 in the Hardware Revision String Characteristic
          lib_aci_set_local_data(&aci_state, PIPE_DEVICE_INFORMATION_HARDWARE_REVISION_STRING_SET,
            (uint8_t *)&(aci_evt->params.cmd_rsp.params.get_device_version), sizeof(aci_evt_cmd_rsp_params_get_device_version_t));
        }
        break;

      case ACI_EVT_CONNECTED:
        Serial.println(F("Evt Connected"));
         
        if( aci_state.bonded != ACI_BOND_STATUS_SUCCESS)
        lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
        aci_state.data_credit_available = aci_state.data_credit_total;
        timing_change_done = false;
        /*
        Get the device version of the nRF8001 and store it in the Hardware Revision String
        */
        //lib_aci_device_version();
        break;

      case ACI_EVT_BOND_STATUS:
        aci_state.bonded = aci_evt->params.bond_status.status_code;
        Serial.println(aci_evt->params.bond_status.status_code,HEX);
        break;

      case ACI_EVT_PIPE_STATUS:
        Serial.println(F("Evt Pipe Status"));

         if((false == timing_change_done) && lib_aci_is_discovery_finished(&aci_state))
         {
                     lib_aci_change_timing_GAP_PPCP();
                      timing_change_done = true;
         }
   
         if ( ACI_BOND_STATUS_SUCCESS == aci_state.bonded && lib_aci_is_discovery_finished(&aci_state)) {
                    // Test ANCS Pipes availability

                    if (lib_aci_is_pipe_closed(&aci_state, PIPE_ANCS_CONTROL_POINT_TX_ACK))
                    {
                        if (!lib_aci_open_remote_pipe(&aci_state, PIPE_ANCS_CONTROL_POINT_TX_ACK))
                        {}
                        //   Serial.println("  -> ANCS Control Point Pipe: Failure opening!");
                        else{}
                         //   Serial.println("  -> ANCS Control Point Pipe: Success opening!");
                    }
//                    else {
//                        Serial.println("  -> ANCS Control Point is opened!");
//                    }
                    if (lib_aci_is_pipe_closed(&aci_state, PIPE_ANCS_DATA_SOURCE_RX))
                    {
                        //Serial.println("  -> ANCS Data Source is closed!");
                        if (!lib_aci_open_remote_pipe(&aci_state, PIPE_ANCS_DATA_SOURCE_RX))
                        {}//    Serial.println("  -> ANCS Data Source Pipe: Failure opening!");
                        else {
                          
//                            Serial.println("  -> ANCS Data Source Pipe: Success opening!");
                        }
                    }
                    else {
                       // Serial.println("  -> ANCS Data Source is opened");
                    }
                    if (lib_aci_is_pipe_closed(&aci_state, PIPE_ANCS_NOTIFICATION_SOURCE_RX)) {
                       // Serial.println("  -> ANCS Notification source is closed!");
                      if (!lib_aci_open_remote_pipe(&aci_state, PIPE_ANCS_NOTIFICATION_SOURCE_RX))
                         { 
                         //  Serial.println("  -> ANCS Notification source: Failure opening!");
                         }
                        else
                        {
                           // Serial.println("  -> ANCS Notification source: Success opening!");
                        }
                    } else {
                        Serial.println("  -> ANCS Notification source is opened!");
                    }
                } else {
                    //Serial.println(" Service Discovery is still going on.");
                }
            
        break;

      case ACI_EVT_TIMING:
      {
            Serial.println("Evt link connection interval changed");
            //Disconnect as soon as we are bonded and required pipes are available
            //This is used to store the bonding info on disconnect and
            //then re-connect to verify the bond
/*             lib_aci_set_local_data(&aci_state,
                                PIPE_COMMUNICATION_SERVICE_COMMUNICATION_LINKING_TIMING_SET,
                                (uint8_t *)&(aci_evt->params.timing.conn_rf_interval),
                                PIPE_COMMUNICATION_SERVICE_COMMUNICATION_LINKING_TIMING_SET_MAX_SIZE);*/
              //bonded_first_time = true;
//
//            ancs_init_state = ANCS_INIT_START_CLOSING_PIPE;
//            bool returned_value = lib_aci_close_remote_pipe(&aci_state, PIPE_ANCS_DATA_SOURCE_RX);
//                    if(returned_value == true) Serial.println("S-a inchis!");
//                    else Serial.println("Nu s-a inchis!");

           // lib_aci_radio_reset();          
            if((ACI_BOND_STATUS_SUCCESS == aci_state.bonded) &&
                    (true == bonded_first_time) &&
                    (GAP_PPCP_MAX_CONN_INT >= aci_state.connection_interval) && 
                    //Timing change already done: Provide time for the the peer to finish
                    (GAP_PPCP_MIN_CONN_INT <= aci_state.connection_interval)) {
                    lib_aci_disconnect(&aci_state, ACI_REASON_TERMINATE);

            }
            break;
      }


      case ACI_EVT_DISCONNECTED:
        Serial.println(F("Evt Disconnected. Link Lost or Advertising timed out"));
        if (ACI_BOND_STATUS_SUCCESS == aci_state.bonded)
        {
          if (ACI_STATUS_EXTENDED == aci_evt->params.disconnected.aci_status) //Link was disconnected
          {
            if (bonded_first_time)
            {
              bonded_first_time = false;
              //Store away the dynamic data of the nRF8001 in the Flash or EEPROM of the MCU
              // so we can restore the bond information of the nRF8001 in the event of power loss
              if (bond_data_read_store(&aci_state))
              {
                Serial.println(F("Dynamic Data read and stored successfully"));
              }
            }
            if (0x24 == aci_evt->params.disconnected.btle_status)
            {
              //The error code appears when phone or Arduino has deleted the pairing/bonding information.
              //The Arduino stores the bonding information in EEPROM, which is deleted only by
              // the user action of connecting pin 6 to 3.3v and then followed by a reset.
              //While deleting bonding information delete on the Arduino and on the phone.
              //Clear the pairing
              Serial.println(F("Pairing/Bonding info cleared from EEPROM."));
              //Address. Value
              EEPROM.write(0, 0);
              lib_aci_disconnect(&aci_state, ACI_REASON_TERMINATE);
              delay(500);
              lib_aci_radio_reset();
            }

          }
          lib_aci_connect(180/* in seconds */, 0x0100 /* advertising interval 100ms*/);
          Serial.println(F("Using existing bond stored in EEPROM."));
        }
        else
        {
          //There is no existing bond. Try to bond.
          lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
          Serial.println(F("Advertising started. Bonding."));
        }
        break;

      case ACI_EVT_DATA_RECEIVED:
      {
        if( aci_evt->params.data_received.rx_data.pipe_number == PIPE_ANCS_NOTIFICATION_SOURCE_RX )
        {
          handle_notification(aci_evt->params.data_received.rx_data.aci_data);
        }
        else
        {
          Serial.print(F("Pipe #"));
           Serial.print(aci_evt->params.data_received.rx_data.pipe_number, DEC);
          Serial.print(F(" Data(Hex) : "));
          for(int i=0; i<aci_evt->len - 2; i++)
          {
            Serial.print((char)aci_evt->params.data_received.rx_data.aci_data[i]);
            Serial.print(F("|"));
          }
          Serial.println();
          int i=0;
          for(i=0; i<aci_evt->len - 2; i++)
          {
            Serial.print(aci_evt->params.data_received.rx_data.aci_data[i],HEX);
            Serial.print(F("|"));
          }
          Serial.println("");
          Serial.println(i,DEC);
        }
        break;
      }
      case ACI_EVT_DATA_CREDIT:

        aci_state.data_credit_available = aci_state.data_credit_available + aci_evt->params.data_credit.credit;
                Serial.print("Data credit ");
        Serial.println(aci_state.data_credit_available,DEC);
        break;
      
     case ACI_EVT_DATA_ACK:
       Serial.println("data ack!");
     break;

      case ACI_EVT_PIPE_ERROR:
        //See the appendix in the nRF8001 Product Specication for details on the error codes
        Serial.print(F("ACI Evt Pipe Error: Pipe #:"));
        //Serial.print(aci_evt->params.pipe_error.pipe_number, DEC);
        
        switch (aci_evt->params.pipe_error.error_code) {
                    case ACI_STATUS_ERROR_BUSY:
                        //Serial.println("  Error Busy");
                        break;
                    case ACI_STATUS_ERROR_PEER_ATT_ERROR:
                        //Serial.println("  Error reported by the peer");
                        break; 
                     
                        //Serial.println("  Pipe Error Code: 0x");
                        //Serial.println(aci_evt->params.pipe_error.error_code, HEX);
                }
        
        //Increment the credit available as the data packet was not sent.
        //The pipe error also represents the Attribute protocol Error Response sent from the peer and that should not be counted
        //for the credit.
        if (ACI_STATUS_ERROR_PEER_ATT_ERROR != aci_evt->params.pipe_error.error_code)
        {
          aci_state.data_credit_available++;
        }
        break;

      case ACI_EVT_HW_ERROR:
        Serial.print(F("HW error: "));
//        Serial.println(aci_evt->params.hw_error.line_num, DEC);
//
//        for(uint8_t counter = 0; counter <= (aci_evt->len - 3); counter++)
//        {
//        Serial.write(aci_evt->params.hw_error.file_name[counter]); //uint8_t file_name[20];
//        }
//        Serial.println();

        //Manage the bond in EEPROM of the AVR
        {
          uint8_t eeprom_status = 0;
          eeprom_status = EEPROM.read(0);
          if (eeprom_status != 0x00)
          {
            Serial.println(F("Previous Bond present. Restoring"));
            //We must have lost power and restarted and must restore the bonding infromation using the ACI Write Dynamic Data
            if (ACI_STATUS_TRANSACTION_COMPLETE == bond_data_restore(&aci_state, eeprom_status, &bonded_first_time))
            {
              Serial.println(F("Bond restored successfully"));
            }
            else
            {
              Serial.println(F("Bond restore failed. Delete the bond and try again."));
            }
          }
        }

        // Start bonding as all proximity devices need to be bonded to be usable
        if (ACI_BOND_STATUS_SUCCESS != aci_state.bonded)
        {
          lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
          Serial.println(F("No Bond present in EEPROM."));
          Serial.println(F("Advertising started : Waiting to be connected and bonded"));
        }
        else
        {
          //connect to an already bonded device
          //Use lib_aci_direct_connect for faster re-connections with PC, not recommended to use with iOS/OS X
          lib_aci_connect(100/* in seconds */, 0x0020 /* advertising interval 20ms*/);
          Serial.println(F("Already bonded : Advertising started : Waiting to be connected"));
        }
        break;
    }
  }
  else
  {
    //Serial.println(F("No ACI Events available"));
    // No event in the ACI Event queue and if there is no event in the ACI command queue the arduino can go to sleep
    // Arduino can go to sleep now
    // Wakeup from sleep from the RDYN line
    
  }
  
  /* setup_required is set to true when the device starts up and enters setup mode.
   * It indicates that do_aci_setup() should be called. The flag should be cleared if
   * do_aci_setup() returns ACI_STATUS_TRANSACTION_COMPLETE.
   */
  if(setup_required)
  {
    if (SETUP_SUCCESS == do_aci_setup(&aci_state))
    {
      setup_required = false;
    }
  }
}

void handle_notification(const unsigned char* data) {
    uint8_t event_id;
    uint8_t event_flags;
    uint8_t category_id;
    uint8_t category_count;
    uint32_t nid;

    unpack(data, "BBBBI", &event_id,
            &event_flags,
            &category_id,
            &category_count,
            &nid);
            
    switch (event_id) {
        case ANCS_EVT_NOTIFICATION_ADDED:
            Serial.println("ADDED");
            switch (category_id) {
                case ANCS_CATEGORY_INCOMING_CALL:
                {
                    Serial.println( "incoming call");
                    break;
                }
                case ANCS_CATEGORY_MISSED_CALL:
                {
                    Serial.println("missed call");
                    break;
                }
                case ANCS_CATEGORY_VOICEMAIL:
                    Serial.println("voicemail call");
                    break;
                case ANCS_CATEGORY_SOCIAL:
                {
                    Serial.println("social msg");                  
                    break;
                }
                case ANCS_CATEGORY_OTHER:
                    Serial.println("other");
                default:
                    Serial.println("Ignored");
                    return;
            }
            break;
        case ANCS_EVT_NOTIFICATION_REMOVED:
            Serial.println("Removed");
            break;
    }
}


/*
Description:

<Add description of the proximity application.

The ACI Evt Data Credit provides the radio level ack of a transmitted packet.
*/
void setup(void)
{
  Serial.begin(115200);
  //Wait until the serial port is available (useful only for the Leonardo)
  //As the Leonardo board is not reseted every time you open the Serial Monitor
  #if defined (__AVR_ATmega32U4__)
    while(!Serial)
    {}
    delay(5000);  //5 seconds delay for enabling to see the start up comments on the serial board
  #elif defined(__PIC32MX__)
    delay(1000);
  #endif

  randomSeed(analogRead(0));
  Serial.println(F("Arduino setup"));

  /**
  Point ACI data structures to the the setup data that the nRFgo studio generated for the nRF8001
  */
  if (NULL != services_pipe_type_mapping)
  {
    aci_state.aci_setup_info.services_pipe_type_mapping = &services_pipe_type_mapping[0];
  }
  else
  {
    aci_state.aci_setup_info.services_pipe_type_mapping = NULL;
  }
  aci_state.aci_setup_info.number_of_pipes    = NUMBER_OF_PIPES;
  aci_state.aci_setup_info.setup_msgs         = setup_msgs;
  aci_state.aci_setup_info.num_setup_msgs     = NB_SETUP_MESSAGES;

  //Tell the ACI library, the MCU to nRF8001 pin connections
  aci_state.aci_pins.board_name = REDBEARLAB_SHIELD_V1_1; //See board.h for details
  aci_state.aci_pins.reqn_pin   = 9;
  aci_state.aci_pins.rdyn_pin   = 8;
  aci_state.aci_pins.mosi_pin   = MOSI;
  aci_state.aci_pins.miso_pin   = MISO;
  aci_state.aci_pins.sck_pin    = SCK;

  aci_state.aci_pins.spi_clock_divider      = SPI_CLOCK_DIV8;//SPI_CLOCK_DIV8  = 2MHz SPI speed
                                                             //SPI_CLOCK_DIV16 = 1MHz SPI speed

  aci_state.aci_pins.reset_pin              = 4; //4 for Nordic board, UNUSED for REDBEARLABS
  aci_state.aci_pins.active_pin             = UNUSED;
  aci_state.aci_pins.optional_chip_sel_pin  = UNUSED;

  aci_state.aci_pins.interface_is_interrupt = false;
  aci_state.aci_pins.interrupt_number       = 1;

  //We reset the nRF8001 here by toggling the RESET line connected to the nRF8001
  //and initialize the data structures required to setup the nRF8001
  //The second parameter is for turning debug printing on for the ACI Commands and Events so they be printed on the Serial
  lib_aci_init(&aci_state, false);
  aci_state.bonded = ACI_BOND_STATUS_FAILED;

  pinMode(6, INPUT); //Pin #6 on Arduino -> PAIRING CLEAR pin: Connect to 3.3v to clear the pairing
  if (0x01 == digitalRead(6))
  {
    //Clear the pairing
    Serial.println(F("Pairing/Bonding info cleared from EEPROM."));
    Serial.println(F("Remove the wire on Pin 6 and reset the board for normal operation."));
    //Address. Value
    EEPROM.write(0, 0);
    while(1) {};
  }
}

bool stringComplete = false;  // whether the string is complete
uint8_t stringIndex = 0;  

void loop()
{
  aci_loop();
  
  // print the string when a newline arrives:
  if (stringComplete)
  {
    Serial.print(F("Sending: "));
    Serial.println((char *)&uart_buffer[0]);
    Serial.print("Number of chars: ");
    Serial.println(stringIndex + 1);

    uart_buffer[stringIndex++] = '\0';
    uart_buffer_len = stringIndex + 1;


    if (!lib_aci_send_data(PIPE_COMMUNICATION_SERVICE_COMMUNICATION_SENT_TX_ACK, uart_buffer, uart_buffer_len))
    {
      Serial.println(F("Serial input dropped"));
    }

    // clear the uart_buffer:
    for (stringIndex = 0; stringIndex < 20; stringIndex++)
    {
      uart_buffer[stringIndex] = '\0';
    }

    // reset the flag and the index in order to receive more data
    stringIndex    = 0;
    stringComplete = false;
  }

  //For ChipKit you have to call the function that reads from Serial
  #if defined (__PIC32MX__)
    if (Serial.available())
    {
      serialEvent();
    }
  #endif
}

void serialEvent() {

  while(Serial.available() > 0){
    // get the new byte:
    dummychar = (uint8_t)Serial.read();
    if(!stringComplete)
    {
      if (dummychar == '\n')
      {
        // if the incoming character is a newline, set a flag
        // so the main loop can do something about it
        stringIndex--;
        stringComplete = true;
      }
      else
      {
        if(stringIndex > 19)
        {
          Serial.println("Serial input truncated");
          stringIndex--;
          stringComplete = true;
        }
        else
        {
          // add it to the uart_buffer
          uart_buffer[stringIndex] = dummychar;
          stringIndex++;
        }
      }
    }
  }
}
