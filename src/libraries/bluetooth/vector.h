#define ANCS_INIT_START_CLOSING_PIPE 1
#define ANCS_INIT_OPENING_DATA_SOURCE 2
#define ANCS_INIT_OPENING_NOTIFICATION_SOURCE 3
#define ANCS_INIT_DONE 4

typedef struct
{
  uint8_t *buffer;
  int size;
} query;

/*
notif_type: tipul notificarii
              1 = apel
              4 = mesaj
caller_id: numele sau numarul persoanei
caller_id_length: lungimea numelui
message: mesajul primit in cazul in care a fost mesaj
message_length: lungimea mesajului
caller_id_index: index folosit pentru constructia caller_id-ului si a mesajului
caller_id_complete: flag activat in momentul in care numele a fost parsat
*/
    
typedef struct
{
	int notif_type;
	uint32_t notif_id;
	char caller_id[20];
	uint16_t caller_id_length;
	char message[30];
	uint16_t message_length;
        uint16_t caller_id_index;
        char caller_id_complete;
} notification;

