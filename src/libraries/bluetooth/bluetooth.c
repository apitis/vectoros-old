
/*

Copyright (c) 2012, 2013 RedBearLab
	
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "hal_platform.h"
#include "asf.h"

#include <boards.h>
#include <lib_aci.h>
#include <aci_setup.h>

#include "conf_board.h"
#include "bluetooth.h"
#include "flash.h"
#include "pack_lib.h"
#include "ancs_base.h"
#include "vector.h"
#include "queue.h"
#include <delay.h>
#include "protocol.h"

void do_buzz();
void goto_sleep();
void attach_ble_interrupt();

#ifdef SERVICES_PIPE_TYPE_MAPPING_CONTENT
    static services_pipe_type_mapping_t
        services_pipe_type_mapping[NUMBER_OF_PIPES] = SERVICES_PIPE_TYPE_MAPPING_CONTENT;
#else
    #define NUMBER_OF_PIPES 0
    static services_pipe_type_mapping_t * services_pipe_type_mapping = NULL;
#endif

/* Store the setup for the nRF8001 in the flash of the AVR to save on RAM */
static hal_aci_data_t setup_msgs[NB_SETUP_MESSAGES] = SETUP_MESSAGES_CONTENT;

/*aci_struct that will contain :
total initial credits
current credit
current state of the aci (setup/standby/active/sleep)
open remote pipe pending
close remote pipe pending
Current pipe available bitmap
Current pipe closed bitmap
Current connection interval, slave latency and link supervision timeout
Current State of the the GATT client (Service Discovery)
Status of the bond (R) Peer address*/
static struct aci_state_t aci_state;

/*Temporary buffers for sending ACI commands*/
static hal_aci_evt_t  aci_data;
static hal_aci_data_t aci_cmd;

/*Timing change state variable*/
static bool timing_change_done = false;

extern int doing_ADC;

/*Initialize the radio_ack. This is the ack received for every transmitted packet.*/
//static bool radio_ack_pending = false;

//The maximum size of a packet is 64 bytes.
#define MAX_TX_BUFF 64
static uint8_t tx_buff[MAX_TX_BUFF];
uint8_t tx_buffer_len = 0;

#define MAX_RX_BUFF 64

uint8_t rx_buff[MAX_RX_BUFF+1];
uint8_t rx_buffer_len = 0;
uint8_t *p_before = &rx_buff[0] ;
uint8_t *p_back = &rx_buff[0];
static unsigned char is_connected = 0;
int device_type = 0;

int nr_of_active_notifications = 0;
char notification_type[256];
int notification_length;
int notification_index;
char notification_details[256];

// VECTOR-P1
static uint8_t reqn_pin = PIO_PA26_IDX, rdyn_pin = PIO_PA25_IDX;
static uint8_t MISO = PIO_PA12_IDX, MOSI = PIO_PA13_IDX, SCK = PIO_PA14_IDX;

int ble_debug = 0;

/*
We will store the bonding info for the nRF8001 in the EEPROM/Flash of the MCU to recover from a power loss situation
*/
static bool bonded_first_time = true;

void ble_interrupt_mode_true()
{
    aci_state.aci_pins.interface_is_interrupt	  = true;
}



void ble_begin()
{
		uint32_t data[4];
		int a = flash_read_unique_id(data,4); 	
		if( a == FLASH_RC_OK )
		{
			//success
		}
		srand(data[3] + data[2] + data[1] + data[0]);

		queue_init();

    /* Point ACI data structures to the the setup data that the nRFgo studio generated for the nRF8001 */   
    if (NULL != services_pipe_type_mapping)
    {
        aci_state.aci_setup_info.services_pipe_type_mapping = &services_pipe_type_mapping[0];
    }
    else
    {
        aci_state.aci_setup_info.services_pipe_type_mapping = NULL;
    }
    
    aci_state.aci_setup_info.number_of_pipes    = NUMBER_OF_PIPES;
    aci_state.aci_setup_info.setup_msgs         = setup_msgs;
    aci_state.aci_setup_info.num_setup_msgs     = NB_SETUP_MESSAGES;

    /*
      Tell the ACI library, the MCU to nRF8001 pin connections.
      The Active pin is optional and can be marked UNUSED
    */	  	
    aci_state.aci_pins.board_name = REDBEARLAB_SHIELD_V1_1; //See board.h for details
    aci_state.aci_pins.reqn_pin   = reqn_pin;
    aci_state.aci_pins.rdyn_pin   = rdyn_pin;
    aci_state.aci_pins.mosi_pin   = MOSI;
    aci_state.aci_pins.miso_pin   = MISO;
    aci_state.aci_pins.sck_pin    = SCK;
	
    aci_state.aci_pins.spi_clock_divider     = 84;
	  
    aci_state.aci_pins.interface_is_interrupt	  = false;

    aci_state.bonded = ACI_BOND_STATUS_FAILED;

    //Turn debug printing on for the ACI Commands and Events to be printed on the Serial
    lib_aci_debug_print(true);

    /*We reset the nRF8001 here by toggling the RESET line connected to the nRF8001
      and initialize the data structures required to setup the nRF8001*/

    lib_aci_init(&aci_state); 
    delay_ms(200);
}

volatile uint8_t ack = 0;

void ble_write(unsigned char data)
{	    
    if(tx_buffer_len == MAX_TX_BUFF)
    {
        return;
    }	
    tx_buff[tx_buffer_len] = data;
    tx_buffer_len++;	
		
}

void ble_write_bytes(unsigned char *data, uint8_t len)
{
    for (int i = 0; i < len; i++)
        ble_write(data[i]);
}

void ble_write_text(unsigned char *data)
{
    if (is_connected)
        ble_write_bytes(data, strlen(data) + 1);
}

int ble_read()
{
	int data;
	if(rx_buffer_len == 0) return -1;
	if(p_before == &rx_buff[MAX_RX_BUFF])
	{
			p_before = &rx_buff[0];
	}
	data = *p_before;
	p_before ++;
	rx_buffer_len--;
	return data;
}

unsigned char ble_available()
{
	return rx_buffer_len;
}

unsigned char ble_connected()
{
    return is_connected;
}

static int cmd_status = 0;
int ble_cmd_status()
{
    return cmd_status;
}


/*
Read the Dymamic data from the EEPROM and send then as ACI Write Dynamic Data to the nRF8001
This will restore the nRF8001 to the situation when the Dynamic Data was Read out
*/
aci_status_code_t bond_data_restore(aci_state_t *aci_stat, uint8_t eeprom_status, bool *bonded_first_time_state)
{
  aci_evt_t *aci_evt;
  uint8_t eeprom_offset_read = 1;
  uint8_t write_dyn_num_msgs = 0;
  uint8_t len = 0;

  // Get the number of messages to write for the eeprom_status
  write_dyn_num_msgs = eeprom_status & 0x7F;
  
  //Read from the EEPROM
  while (1)
  {
    len = EEPROM_read(eeprom_offset_read);
    eeprom_offset_read++;
    aci_cmd.buffer[0] = len;

    for (uint8_t i=1; i<=len; i++)
    {
        aci_cmd.buffer[i] = EEPROM_read(eeprom_offset_read);
        eeprom_offset_read++;
    }
    //Send the ACI Write Dynamic Data
    if (!hal_aci_tl_send(&aci_cmd))
    {
      return ACI_STATUS_ERROR_INTERNAL;
    }

    //Spin in the while loop waiting for an event
    while (1)
    {
      if (lib_aci_event_get(aci_stat, &aci_data))
      {
        aci_evt = &aci_data.evt;

        if (ACI_EVT_CMD_RSP != aci_evt->evt_opcode)
        {
          //Got something other than a command response evt -> Error
          return ACI_STATUS_ERROR_INTERNAL;
        }
        else
        {
          write_dyn_num_msgs--;

          //ACI Evt Command Response
          if (ACI_STATUS_TRANSACTION_COMPLETE == aci_evt->params.cmd_rsp.cmd_status)
          {
            //Set the state variables correctly
            *bonded_first_time_state = false;
            aci_stat->bonded = ACI_BOND_STATUS_SUCCESS;
            delay_ms(10);

            return ACI_STATUS_TRANSACTION_COMPLETE;
          }
          if (0 >= write_dyn_num_msgs)
          {
            //should have returned earlier
            return ACI_STATUS_ERROR_INTERNAL;
          }
          if (ACI_STATUS_TRANSACTION_CONTINUE == aci_evt->params.cmd_rsp.cmd_status)
          {
            //break and write the next ACI Write Dynamic Data
            break;
          }
        }
      }
    }
  }
}

/*
This function is specific to the atmega328
@params ACI Command Response Evt received from the Read Dynmaic Data
*/
void bond_data_store(aci_evt_t *evt)
{
  static int eeprom_write_offset = 1;

  //Write it to non-volatile storage
  EEPROM_write( eeprom_write_offset, evt->len -2 );
  eeprom_write_offset++;

  EEPROM_write( eeprom_write_offset, ACI_CMD_WRITE_DYNAMIC_DATA);
  eeprom_write_offset++;

  for (uint8_t i=0; i< (evt->len-3); i++)
  {
    EEPROM_write( eeprom_write_offset, evt->params.cmd_rsp.params.padding[i]);
    eeprom_write_offset++;
  }
}

bool once = false;

bool bond_data_read_store(aci_state_t *aci_stat)
{
  /*
  The size of the dynamic data for a specific Bluetooth Low Energy configuration
  is present in the ublue_setup.gen.out.txt generated by the nRFgo studio as "dynamic data size".
  */
  bool status = false;
  aci_evt_t * aci_evt = NULL;
  uint8_t read_dyn_num_msgs = 0;

  //Start reading the dynamic data
  lib_aci_read_dynamic_data();
  read_dyn_num_msgs++;

  while (1)
  {
    if (true == lib_aci_event_get(aci_stat, &aci_data))
    {
      aci_evt = &aci_data.evt;

      if (ACI_EVT_CMD_RSP != aci_evt->evt_opcode )
      {
        //Got something other than a command response evt -> Error
        status = false;
        break;
      }

      if (ACI_STATUS_TRANSACTION_COMPLETE == aci_evt->params.cmd_rsp.cmd_status)
      {
        //Store the contents of the command response event in the EEPROM
        //(len, cmd, seq-no, data) : cmd ->Write Dynamic Data so it can be used directly
        bond_data_store(aci_evt);

        //Set the flag in the EEPROM that the contents of the EEPROM is valid
        EEPROM_write(0, 0x80|read_dyn_num_msgs );
        //Finished with reading the dynamic data
        status = true;

        break;
      }

      if (!(ACI_STATUS_TRANSACTION_CONTINUE == aci_evt->params.cmd_rsp.cmd_status))
      {
        //We failed the read dymanic data
        //Set the flag in the EEPROM that the contents of the EEPROM is invalid
        EEPROM_write(0, 0x00);

        status = false;
        break;
      }
      else
      {
        //Store the contents of the command response event in the EEPROM
        // (len, cmd, seq-no, data) : cmd ->Write Dynamic Data so it can be used directly when re-storing the dynamic data
        bond_data_store(aci_evt);

        //Read the next dynamic data message
        lib_aci_read_dynamic_data();
        read_dyn_num_msgs++;
      }
    }
  }
  return status;
}



const char *ANCS_NAME[] = {
    "Other",
    "Incoming",
    "Missed",
    "Voicemail",
    "Social",
    "Schedule",
    "Email",
    "News",
    "Fitness",
    "Business",
    "Location",
    "Entertainment",
};


void handle_notification(const unsigned char* data)
{
    uint8_t event_id;
    uint8_t event_flags;
    uint8_t category_id;
    uint8_t category_count;
    uint32_t nid;


    unpack(data, "BBBBI", &event_id,
           &event_flags,
           &category_id,
           &category_count,
           &nid);
    
    char command[8];
    
    switch (event_id)
    {
        case ANCS_EVT_NOTIFICATION_ADDED:
            switch (category_id)
            {
                case ANCS_CATEGORY_INCOMING_CALL:
                case ANCS_CATEGORY_SCHEDULE:
                case ANCS_CATEGORY_SOCIAL:
                    do_buzz();
                    break;
            }

            nr_of_active_notifications++;
            pack(command, "BIBH", ANCS_COMMAND_GET_NOTIF_ATTRIBUTES, nid,
                 ANCS_NOTIFICATION_ATTRIBUTE_TITLE,ANCS_NOTIFICATION_ATTRIBUTE_DATA_SIZE);
            queue_push_element(command);
            strcpy(notification_type,
                   category_id >= ANCS_LAST_CATEGORY ? "WTF?" : ANCS_NAME[category_id]);

            break;
            
        case ANCS_EVT_NOTIFICATION_REMOVED:
            notification_type[0] = '\0';
            notification_details[0] = '\0';
            nr_of_active_notifications--;
            break;
    }

    
}



int init_ble_int = 1;


void process_events()
{
  static bool setup_required = false;

  // We enter the if statement only when there is a ACI event available to be processed
  if (lib_aci_event_get(&aci_state, &aci_data))
  {
    aci_evt_t * aci_evt;
    aci_evt = &aci_data.evt;

    switch(aci_evt->evt_opcode)
    {
      /**
      As soon as you reset the nRF8001 you will get an ACI Device Started Event
      */
      case ACI_EVT_DEVICE_STARTED:
      {
          aci_state.data_credit_total = aci_evt->params.device_started.credit_available;

          switch(aci_evt->params.device_started.device_mode)
          {
              case ACI_DEVICE_SETUP:
                  /**
                     When the device is in the setup mode
                  */
                  /* Serial.println(F("Evt Device Started: Setup")); */
                
                  if (ACI_STATUS_TRANSACTION_COMPLETE != do_aci_setup(&aci_state))
                  {
                      //FIXME: puts("Error in ACI Setup\n\r");
                  }
                  
                  break;

              case ACI_DEVICE_STANDBY:
                  /* Serial.println(F("Evt Device Started: Standby")); */
                  if (aci_evt->params.device_started.hw_error)
                  {
                      delay_ms(20); //Magic number used to make sure the HW error event is handled correctly.
                  }
                  else
                  {
                      //Manage the bond in EEPROM of the AVR
                      uint8_t eeprom_status = 0;
                      eeprom_status = EEPROM_read(0);
                    
                      //ble_debug = eeprom_status;
    
                      if (eeprom_status != 0x00 && eeprom_status != 0xFF)
                      {
                          /* Serial.println(F("Previous Bond present. Restoring")); */
                          //We must have lost power and restarted and must restore the bonding infromation using the ACI Write Dynamic Data

                          if (ACI_STATUS_TRANSACTION_COMPLETE == bond_data_restore(&aci_state, eeprom_status, &bonded_first_time))
                          {
                              /* Serial.println(F("Bond restored successfully")); */
//                              screen_put_text_at("BOND", 15, 10);	
                          }
                          else
                          {
//                              screen_put_text_at("FAIL", 15, 10);	
                              /* Serial.println(F("Bond restore failed. Delete the bond and  try again."));*/
                          }
                      }

                      // Start bonding as all proximity devices need to be bonded to be usable
                      if (ACI_BOND_STATUS_SUCCESS != aci_state.bonded)
                      {	
                          char device_name[5] = "Ve";
                          device_name[2] = (char)(((int)'0')+rand() % 9);
                          device_name[3] = (char)(((int)'0')+rand() % 9);
                          device_name[4] = (char)(((int)'0')+rand() % 9);                
                          if(lib_aci_set_local_data(&aci_state, PIPE_GAP_DEVICE_NAME_SET,(uint8_t *)device_name, 5))
                          {
                              screen_put_text_at(device_name, 15, 10);	
                          }
	
                          lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
                          /* Serial.println(F("No Bond present in EEPROM.")); */
                      }
                      else
                      {
                          //connect to an already bonded device
                          //Use lib_aci_direct_connect for faster re-connections with PC, not recommended to use with iOS/OS X
                          lib_aci_connect(100/* in seconds */, 0x0020 /* advertising interval 20ms*/);
                          /* Serial.println(F("Already bonded : Advertising started : Waiting to be connected")); */
                          delay_ms(100);
                          if (init_ble_int)
                          {
                              init_ble_int = 0;
                              attach_ble_interrupt();
                              ble_interrupt_mode_true();
                          }
                      }

                  }
                  break;
          }
      }
      break; //ACI Device Started Event

      case ACI_EVT_CMD_RSP:
        //If an ACI command response event comes with an error -> stop
        if (ACI_STATUS_SUCCESS != aci_evt->params.cmd_rsp.cmd_status)
        {
            cmd_status = aci_evt->params.cmd_rsp.cmd_status;
            //ACI ReadDynamicData and ACI WriteDynamicData will have status codes of
            //TRANSACTION_CONTINUE and TRANSACTION_COMPLETE
            //all other ACI commands will have status code of ACI_STATUS_SCUCCESS for a successful command
            /* Serial.print(F("ACI Command ")); */
            /* Serial.println(aci_evt->params.cmd_rsp.cmd_opcode, HEX); */
            /* Serial.print(F("Evt Cmd respone: Status ")); */
            /* Serial.println(aci_evt->params.cmd_rsp.cmd_status, HEX); */
             if( ACI_STATUS_ERROR_CMD_UNKNOWN == aci_evt->params.cmd_rsp.cmd_status)
             { 
            /*    Serial.println("CMD UNKNOWN"); */
									ble_debug = 999; 
                 //ar trebui un hardware reset aici
             } 
							else
							{
								ble_debug = aci_evt->params.cmd_rsp.cmd_status; 		
							}
            // IGNORE ERROR FOR NOW - FIXME - FIGURE OUT WHY
            break;
        }

        if (ACI_CMD_GET_DEVICE_VERSION == aci_evt->params.cmd_rsp.cmd_opcode)
        {
          //Store the version and configuration information of the nRF8001 in the Hardware Revision String Characteristic
            lib_aci_set_local_data(
                &aci_state,
                PIPE_DEVICE_INFORMATION_HARDWARE_REVISION_STRING_SET,
                (uint8_t *)&(aci_evt->params.cmd_rsp.params.get_device_version),
                sizeof(aci_evt_cmd_rsp_params_get_device_version_t));
        }
        break;

      case ACI_EVT_CONNECTED:
        /* Serial.println(F("Evt Connected")); */

        is_connected = 1;

        if( aci_state.bonded != ACI_BOND_STATUS_SUCCESS)
            lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);

        aci_state.data_credit_available = aci_state.data_credit_total;

        timing_change_done = false;
        
        /*
        Get the device version of the nRF8001 and store it in the Hardware Revision String
        */
        //FIXME: lib_aci_device_version();
        break;

      case ACI_EVT_BOND_STATUS:
        aci_state.bonded = aci_evt->params.bond_status.status_code;
        /* Serial.println(aci_evt->params.bond_status.status_code,HEX); */
        break;

      case ACI_EVT_PIPE_STATUS:
        /* Serial.println(F("Evt Pipe Status")); */
          if((false == timing_change_done) /*&& lib_aci_is_discovery_finished(&aci_state)*/)
          {
              lib_aci_change_timing_GAP_PPCP();
              timing_change_done = true;
          }
   
          if ( ACI_BOND_STATUS_SUCCESS == aci_state.bonded && lib_aci_is_discovery_finished(&aci_state))
          {
              // Test ANCS Pipes availability


              if (lib_aci_is_pipe_closed(&aci_state, PIPE_ANCS_CONTROL_POINT_TX_ACK))
              {
                  if (!lib_aci_open_remote_pipe(&aci_state, PIPE_ANCS_CONTROL_POINT_TX_ACK))
                  {
                      //   Serial.println("  -> ANCS Control Point Pipe: Failure opening!");
                  }
                  else
                  {
                      //   Serial.println("  -> ANCS Control Point Pipe: Success opening!");
                  }
              }
              else
              {
                  /* Serial.println("  -> ANCS Control Point is opened!"); */
              }

              if (lib_aci_is_pipe_closed(&aci_state, PIPE_ANCS_DATA_SOURCE_RX))
              {
                  //Serial.println("  -> ANCS Data Source is closed!");
                  if (!lib_aci_open_remote_pipe(&aci_state, PIPE_ANCS_DATA_SOURCE_RX))
                  {
                      //    Serial.println("  -> ANCS Data Source Pipe: Failure opening!");
                  }
                  else
                  {
                      // Serial.println("  -> ANCS Data Source Pipe: Success opening!");
                  }
              }
              else
              {
                  // Serial.println("  -> ANCS Data Source is opened");
              }

              if (lib_aci_is_pipe_closed(&aci_state, PIPE_ANCS_NOTIFICATION_SOURCE_RX))
              {
                  // Serial.println("  -> ANCS Notification source is closed!");
                  if (!lib_aci_open_remote_pipe(&aci_state, PIPE_ANCS_NOTIFICATION_SOURCE_RX))
                  { 
                      //  Serial.println("  -> ANCS Notification source: Failure opening!");
                  }
                  else
                  {
                      // Serial.println("  -> ANCS Notification source: Success opening!");
                  }
              }
              else
              {
                  /* Serial.println("  -> ANCS Notification source is opened!"); */
              }
          }
          else
          {
              //Serial.println(" Service Discovery is still going on.");
          }
            
        break;

      case ACI_EVT_TIMING:
          /* Serial.println("Evt link connection interval changed"); */

          if((ACI_BOND_STATUS_SUCCESS == aci_state.bonded) &&
             (true == bonded_first_time) &&
             (GAP_PPCP_MAX_CONN_INT >= aci_state.connection_interval) && 
             //Timing change already done: Provide time for the the peer to finish
             (GAP_PPCP_MIN_CONN_INT <= aci_state.connection_interval))
          {
              lib_aci_disconnect(&aci_state, ACI_REASON_TERMINATE);
              
          }

          break;


      case ACI_EVT_DISCONNECTED:
        /* Serial.println(F("Evt Disconnected. Link Lost or Advertising timed out")); */

        is_connected = 0;
        ack = 1;

        if (ACI_BOND_STATUS_SUCCESS == aci_state.bonded)
        {
            if (ACI_STATUS_EXTENDED == aci_evt->params.disconnected.aci_status) //Link was disconnected
            {
                if (bonded_first_time)
                {
                    bonded_first_time = false;
                    //Store away the dynamic data of the nRF8001 in the Flash or EEPROM of the MCU
                    // so we can restore the bond information of the nRF8001 in the event of power loss
                    if (bond_data_read_store(&aci_state))
                    {
                        /* Serial.println(F("Dynamic Data read and stored successfully")); */
                    }
                }

                if (0x24 == aci_evt->params.disconnected.btle_status)
                {
                    //The error code appears when phone or Arduino has deleted the pairing/bonding information.
                    //The Arduino stores the bonding information in EEPROM, which is deleted only by
                    // the user action of connecting pin 6 to 3.3v and then followed by a reset.
                    //While deleting bonding information delete on the Arduino and on the phone.
                    //Clear the pairing
                    /* Serial.println(F("Pairing/Bonding info cleared from EEPROM.")); */
                    //Address. Value
                    EEPROM_write(0, 0);
                    lib_aci_disconnect(&aci_state, ACI_REASON_TERMINATE);
                    delay_ms(500);
                    lib_aci_radio_reset();
                }

            }
            lib_aci_connect(180/* in seconds */, 0x0100 /* advertising interval 100ms*/);

            delay_ms(100);
            
            if (init_ble_int)
            {
                init_ble_int = 0;
            	attach_ble_interrupt();
                ble_interrupt_mode_true();
            }
//            Serial.println(F("Using existing bond stored in EEPROM."));
        }
        else
        {
            //There is no existing bond. Try to bond.
            lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
//            Serial.println(F("Advertising started. Bonding."));
        }
        break;

        case ACI_EVT_DATA_RECEIVED:
            if( aci_evt->params.data_received.rx_data.pipe_number == PIPE_ANCS_NOTIFICATION_SOURCE_RX )
            {
                handle_notification(aci_evt->params.data_received.rx_data.aci_data);
                device_type = DEVICE_TYPE_IOS; 
            }
            else if(aci_evt->params.data_received.rx_data.pipe_number == PIPE_ANCS_DATA_SOURCE_RX || 
                    aci_evt->params.data_received.rx_data.pipe_number == PIPE_COMMUNICATION_SERVICE_COMMUNICATION_DETAILS_RX 
                    )
            {
                if (notification_length == 0)
                {
                    notification_length = aci_evt->params.data_received.rx_data.aci_data[6];

                    if (notification_length <= 12)
                    {
                        memcpy(notification_details,
                               aci_evt->params.data_received.rx_data.aci_data + 8,
                               notification_length);

                        notification_index = notification_length;
                    }
                    else
                    {
                        memcpy(notification_details,
                               aci_evt->params.data_received.rx_data.aci_data + 8,
                               12);

                        notification_index = 12;
                    }

                    notification_details[notification_index] = '\0';
                }
                else
                {
                    if (notification_length - notification_index > 20)
                    {
                        memcpy(notification_details + notification_index,
                               aci_evt->params.data_received.rx_data.aci_data,
                               20);

                        notification_index += 20;

                        notification_details[notification_index] = '\0';
                    }
                    else
                    {
                        memcpy(notification_details + notification_index,
                               aci_evt->params.data_received.rx_data.aci_data,
                               notification_length - notification_index);

                        notification_details[notification_length] = '\0';
                    }
                }
            } 
            else if(aci_evt->params.data_received.rx_data.pipe_number == PIPE_COMMUNICATION_SERVICE_COMMUNICATION_RECEIVE_RX  )
            {

		if( aci_evt->params.data_received.rx_data.aci_data[0] == 0 || 
		    aci_evt->params.data_received.rx_data.aci_data[0] == 1 ||
		    aci_evt->params.data_received.rx_data.aci_data[0] == 2 )
		{
		    handle_notification(aci_evt->params.data_received.rx_data.aci_data);
                    device_type = DEVICE_TYPE_ANDROID; 
		}
		else
                {
                  for(int i = 0; i < aci_evt->len - 2; i++)
                  {
                      if(rx_buffer_len == MAX_RX_BUFF) 
                     {	
                          break;
                      }
                      else
                      {
                          if(p_back == &rx_buff[MAX_RX_BUFF])
                         {
                              p_back = &rx_buff[0];
                          }
                          *p_back = aci_evt->params.data_received.rx_data.aci_data[i];							
                          rx_buffer_len++;
                          p_back++;
                      }
                  }
                }
            }
            break;

    case ACI_EVT_DATA_CREDIT:

        aci_state.data_credit_available = aci_state.data_credit_available + aci_evt->params.data_credit.credit;
        /* Serial.print("Data credit "); */
        /* Serial.println(aci_state.data_credit_available,DEC); */
        ack = 1;
        break;
      
     case ACI_EVT_DATA_ACK:
       /* Serial.println("data ack!"); */
     break;

      case ACI_EVT_PIPE_ERROR:
        //See the appendix in the nRF8001 Product Specication for details on the error codes
        /* Serial.print(F("ACI Evt Pipe Error: Pipe #:")); */
        //Serial.print(aci_evt->params.pipe_error.pipe_number, DEC);
        switch (aci_evt->params.pipe_error.error_code) {
                    case ACI_STATUS_ERROR_BUSY:
                        //Serial.println("  Error Busy");
                        break;
                    case ACI_STATUS_ERROR_PEER_ATT_ERROR:
                        //Serial.println("  Error reported by the peer");
                        break; 
                     
                        //Serial.println("  Pipe Error Code: 0x");
                        //Serial.println(aci_evt->params.pipe_error.error_code, HEX);
                }
        
        //Increment the credit available as the data packet was not sent.
        //The pipe error also represents the Attribute protocol Error Response sent from the peer and that should not be counted
        //for the credit.
        if (ACI_STATUS_ERROR_PEER_ATT_ERROR != aci_evt->params.pipe_error.error_code)
        {
          aci_state.data_credit_available++;
        }
        break;

      case ACI_EVT_HW_ERROR:
        /* Serial.print(F("HW error: ")); */
//        Serial.println(aci_evt->params.hw_error.line_num, DEC);
//
//        for(uint8_t counter = 0; counter <= (aci_evt->len - 3); counter++)
//        {
//        Serial.write(aci_evt->params.hw_error.file_name[counter]); //uint8_t file_name[20];
//        }
//        Serial.println();

        //Manage the bond in EEPROM of the AVR
        {
          uint8_t eeprom_status = 0;
          eeprom_status = EEPROM_read(0);
          if (eeprom_status != 0x00 && eeprom_status != 0xFF)
          {
            /* Serial.println(F("Previous Bond present. Restoring")); */
            //We must have lost power and restarted and must restore the bonding infromation using the ACI Write Dynamic Data
            if (ACI_STATUS_TRANSACTION_COMPLETE == bond_data_restore(&aci_state, eeprom_status, &bonded_first_time))
            {
              /* Serial.println(F("Bond restored successfully")); */
            }
            else
            {
              /* Serial.println(F("Bond restore failed. Delete the bond and try again.")); */
            }
          }
        }

        // Start bonding as all proximity devices need to be bonded to be usable
        if (ACI_BOND_STATUS_SUCCESS != aci_state.bonded)
        {
          lib_aci_bond(180/* in seconds */, 0x0050 /* advertising interval 50ms*/);
          /* Serial.println(F("No Bond present in EEPROM.")); */
          /* Serial.println(F("Advertising started : Waiting to be connected and bonded")); */
        }
        else
        {
          //connect to an already bonded device
          //Use lib_aci_direct_connect for faster re-connections with PC, not recommended to use with iOS/OS X
          lib_aci_connect(100/* in seconds */, 0x0020 /* advertising interval 20ms*/);
          /* Serial.println(F("Already bonded : Advertising started : Waiting to be connected")); */
        }
        break;
    }
  }
  else
  {

      if( queue_size() > 0 && aci_state.data_credit_available > 0)
      {
          char *command = queue_pop_element();
          if( device_type == DEVICE_TYPE_IOS )
          {
            lib_aci_send_data(PIPE_ANCS_CONTROL_POINT_TX_ACK, command, 8);
          }
          else if( device_type == DEVICE_TYPE_ANDROID ) 
          {
            lib_aci_send_data(PIPE_COMMUNICATION_SERVICE_COMMUNICATION_SENT_TX, command, 8);
          }
          notification_length = 0;
          notification_index = 0;
          notification_details[0] = '\0';
          //delay_ms(400);
      }

      // No events in the ACI Event queue
      // Vector can go to sleep now
      // Wakeup from sleep from the RDYN line
      if (aci_state.aci_pins.interface_is_interrupt)//FIXMENOW && is_connected)
      {
          goto_sleep();
      }
  }
}


void ble_do_events()
{
    if (lib_aci_is_pipe_available(&aci_state, PIPE_COMMUNICATION_SERVICE_COMMUNICATION_SENT_TX))
    {
        unsigned char Index = 0;

        while(tx_buffer_len > 0)
        {
            int curr_bytes = tx_buffer_len > 20 ? 20 : tx_buffer_len;
                
            if(true == lib_aci_send_data(PIPE_COMMUNICATION_SERVICE_COMMUNICATION_SENT_TX, &tx_buff[Index], curr_bytes))
            {
                //FIXME: printf("data transmmit success!  Length: 20\n\r");
            	tx_buffer_len -= curr_bytes;
            	Index += curr_bytes;
            	aci_state.data_credit_available--;
            	//FIXME: printf("Data Credit available: %d\n\r", aci_state.data_credit_available);

            	ack = 0;
            	while (!ack)
             	{
	          process_events();
           	}
	    }
            else
            {
                //FIXME: puts("data transmmit fail !\n\r");
            }
            
        }

    }
    process_events();
}

