#include "queue.h"

#define QUEUE_SIZE 10
#define QUEUE_ELEMENT_SIZE 8

char queue[QUEUE_SIZE][QUEUE_ELEMENT_SIZE];
char head;
char tail;
char count;

void queue_init()
{
	head = 0;
	tail = 0;
	count = 0;
}

int queue_size()
{
	return count;
}

void queue_push_element(char *element)
{
	if( tail == QUEUE_SIZE )
	{
		tail = 0;
	}

	int i = 0;
	for(i=0;i<QUEUE_ELEMENT_SIZE;i++)
	{
		queue[tail][i] = element[i];
	}

	if( count > QUEUE_SIZE )
	{
		count = QUEUE_SIZE;
	}
	else
	{
		count++;
	}
	tail++;
}

char* queue_pop_element()
{
	if( head >= QUEUE_SIZE )
	{
		head = 0;
	} 
	
	if(count == 0)
	{
		return NULL;
	}

	char *element = queue[head];
	head++;
	count--;

	return element;
}
