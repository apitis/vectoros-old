/* Copyright (c) 2009 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT. 
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 * $LastChangedRevision: 4808 $
 */ 

/** @file
@brief Implementation of the ACI transport layer module
*/

#include "hal_platform.h"
#include "hal_aci_tl.h"

#include "asf.h"
#include "compiler.h"
#include "interrupt.h"
#include "spi.h"
#include "pio.h"
#include <delay.h>

#include "conf_board.h"


static void           m_print_aci_data(hal_aci_data_t *p_data);
static uint8_t        spi_readwrite(uint8_t aci_byte);

static hal_aci_data_t received_data;
static bool           aci_debug_print;

aci_queue_t    aci_tx_q;
aci_queue_t    aci_rx_q;

static aci_pins_t	 *a_pins_local_ptr;

static void m_aci_q_init(aci_queue_t *aci_q)
{
    uint8_t loop;
  
//  aci_debug_print = false;
    aci_q->head = 0;
    aci_q->tail = 0;
    for(loop=0; loop<ACI_QUEUE_SIZE; loop++)
    {
        aci_q->aci_data[loop].buffer[0] = 0x00;
        aci_q->aci_data[loop].buffer[1] = 0x00;
    }
}

void hal_aci_debug_print(bool enable)
{
	aci_debug_print = enable;
}

bool m_aci_q_enqueue(aci_queue_t *aci_q, hal_aci_data_t *p_data)
{
    const uint8_t next = (aci_q->tail + 1) % ACI_QUEUE_SIZE;
    const uint8_t length = p_data->buffer[0];
  
    if (next == aci_q->head)
    {
        /* full queue */
        return false;
    }
    aci_q->aci_data[aci_q->tail].status_byte = 0;
  
    memcpy((uint8_t *)&(aci_q->aci_data[aci_q->tail].buffer[0]), (uint8_t *)&p_data->buffer[0], length + 1);
    aci_q->tail = next;
  
    return true;
}

//@comment after a port to a new mcu have test for the queue states, esp. the full and the empty states
static bool m_aci_q_dequeue(aci_queue_t *aci_q, hal_aci_data_t *p_data)
{
    if (aci_q->head == aci_q->tail)
    {
        /* empty queue */
        return false;
    }
  
    memcpy((uint8_t *)p_data, (uint8_t *)&(aci_q->aci_data[aci_q->head]), sizeof(hal_aci_data_t));
    aci_q->head = (aci_q->head + 1) % ACI_QUEUE_SIZE;
  
    return true;
}

bool m_aci_q_is_empty(aci_queue_t *aci_q)
{
    return (aci_q->head == aci_q->tail);
}

bool m_aci_q_is_full(aci_queue_t *aci_q)
{
    uint8_t next;
    bool state;
  
    //This should be done in a critical section
    cpu_irq_disable();
    next = (aci_q->tail + 1) % ACI_QUEUE_SIZE;  
  
    if (next == aci_q->head)
    {
        state = true;
    }
    else
    {
        state = false;
    }

    cpu_irq_enable();
    //end
  
    return state;
}



void m_print_aci_data(hal_aci_data_t *p_data)
{
    const uint8_t length = p_data->buffer[0];
    uint8_t i;
  
    //FIXME: printf("%02d: ", length);

    //FIXME: for (i=0; i<=length; i++)
        //FIXME: printf("%02X, ", p_data->buffer[i]);

    //FIXME: printf("\n\r");
}

void m_rdy_line_handle(void)
{
    hal_aci_data_t *p_aci_data;
  
    //FIXME: detachInterrupt(1);????????????????
  
    // Receive or transmit data
    p_aci_data = hal_aci_tl_poll_get();
  
    // Check if we received data
    if (p_aci_data->buffer[0] > 0)
    {
        if (!m_aci_q_enqueue(&aci_rx_q, p_aci_data))
        {
            /* Receive Buffer full.
               Should never happen.
               Spin in a while loop.
            */	  
            while(1);
        }
        if (m_aci_q_is_full(&aci_rx_q))
        {
            /* Disable RDY line interrupt.
               Will latch ??????????? any pending RDY lines, so when enabled it again this
               routine should be taken again */
            if (true == a_pins_local_ptr->interface_is_interrupt)
            {
                pio_disable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
            }
        }    
    }
}

bool hal_aci_tl_event_get(hal_aci_data_t *p_aci_data)
{
    bool was_full = m_aci_q_is_full(&aci_rx_q);
  
    if (m_aci_q_dequeue(&aci_rx_q, p_aci_data))
    {
        if (true == aci_debug_print)
        {
            //FIXME: printf(" E");
            m_print_aci_data(p_aci_data);
        }
    
        if (was_full)
        {
            if (true == a_pins_local_ptr->interface_is_interrupt)
            {
		/* Enable RDY line interrupt again */
                pio_enable_interrupt(PIN_RDYN_PIO, PIN_RDYN_MASK);
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

void hal_aci_tl_init(aci_pins_t *a_pins)
{
    received_data.buffer[0] = 0;
  
    m_aci_pins_set(a_pins);

    /* initialize aci cmd queue */
    m_aci_q_init(&aci_tx_q);  
    m_aci_q_init(&aci_rx_q);

    //Configure the IO lines
    pio_configure_pin(a_pins->reqn_pin, PIO_TYPE_PIO_OUTPUT_1);
    pio_configure_pin(a_pins->rdyn_pin, PIO_INPUT | PIO_PULLUP);

/*    
    if (UNUSED != a_pins->active_pin)
    {
	//pinMode(a_pins->active_pin,	INPUT);  
    }
  
    if (UNUSED != a_pins->reset_pin)
    {
        pio_configure_pin(a_pins->reset_pin, PIO_TYPE_PIO_OUTPUT_1);

        pio_set_pin_high(a_pins->reset_pin);
        pio_set_pin_low(a_pins->reset_pin);
        pio_set_pin_high(a_pins->reset_pin);
    }
*/
  
    delay_ms(100); //Wait for the nRF8001 to get hold of its lines - the lines float for a few ms after the reset
  
    //Attach the interrupt to the RDYN line as requested by the caller
    if (a_pins->interface_is_interrupt)
    {
        // it is done in main as the setup cannot be done in interrupts mode for some strange reason
        //attach_ble_interrupt(); 
	//attachInterrupt(a_pins->interrupt_number, m_rdy_line_handle, LOW); 
    }  
}

bool hal_aci_tl_send(hal_aci_data_t *p_aci_cmd)
{
    const uint8_t length = p_aci_cmd->buffer[0];
    bool ret_val = false;

    if (length > HAL_ACI_MAX_LENGTH)
        return false;
    else
        if (m_aci_q_enqueue(&aci_tx_q, p_aci_cmd))
            ret_val = true;

    if (true == aci_debug_print)
    {
        //FIXME: printf("C");
        m_print_aci_data(p_aci_cmd);
    }
  
    pio_set_pin_low(a_pins_local_ptr->reqn_pin);

    return ret_val;
}


hal_aci_data_t * hal_aci_tl_poll_get(void)
{
    uint8_t byte_cnt;
    uint8_t byte_sent_cnt;
    uint8_t max_bytes;
    hal_aci_data_t data_to_send;


    pio_set_pin_low(a_pins_local_ptr->reqn_pin);
  
    // Receive from queue
    if (m_aci_q_dequeue(&aci_tx_q, &data_to_send) == false)
    {
        /* queue was empty, nothing to send */
        data_to_send.status_byte = 0;
        data_to_send.buffer[0] = 0;
    }
  
    //Change this if your mcu has DMA for the master SPI
  
    // Send length, receive header
    byte_sent_cnt = 0;
    received_data.status_byte = spi_readwrite(data_to_send.buffer[byte_sent_cnt++]);
    // Send first byte, receive length from slave
    received_data.buffer[0] = spi_readwrite(data_to_send.buffer[byte_sent_cnt++]);

    if (0 == data_to_send.buffer[0])
    {
        max_bytes = received_data.buffer[0];
    }
    else
    {
        // Set the maximum to the biggest size. One command byte is already sent
        max_bytes = (received_data.buffer[0] > (data_to_send.buffer[0] - 1)) 
            ? received_data.buffer[0] : (data_to_send.buffer[0] - 1);
    }
  
    if (max_bytes > HAL_ACI_MAX_LENGTH)
        max_bytes = HAL_ACI_MAX_LENGTH;
  
    // Transmit/receive the rest of the packet
    for (byte_cnt = 0; byte_cnt < max_bytes; byte_cnt++)
        received_data.buffer[byte_cnt + 1] = spi_readwrite(data_to_send.buffer[byte_sent_cnt++]);

    pio_set_pin_high(a_pins_local_ptr->reqn_pin);
    //RDYN should follow the REQN line in approx 100ns
  
#if defined(__SAM3X8E__)
    // nothing here, but we need to do as this, compiler bug?
#else
//  sleep_enable();
#endif
  
    if (a_pins_local_ptr->interface_is_interrupt)
    {
        //attach_ble_interrupt();

	//FIXME: attachInterrupt(a_pins_local_ptr->interrupt_number, m_rdy_line_handle, LOW);	  
    }

    if (false == m_aci_q_is_empty(&aci_tx_q))
    {
        //Lower the REQN line to start a new ACI transaction         
        pio_set_pin_low(a_pins_local_ptr->reqn_pin);
    }
  
    /* valid Rx available or transmit finished*/
    return (&received_data);
}

unsigned char reverse[] = {
    0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
    0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
    0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
    0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
    0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
    0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
    0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
    0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
    0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
    0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
    0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
    0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
    0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
    0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
    0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
    0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
    0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
    0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
    0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
    0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
    0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
    0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
    0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
    0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
    0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
    0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
    0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
    0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
    0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
    0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
    0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
    0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
};

static uint8_t spi_readwrite(const uint8_t aci_byte)
{
    uint16_t data = 0;
    uint8_t uc_pcs = 0;
    
    spi_write(SPI_MASTER_BASE, reverse[aci_byte], 0, 0);
    spi_read(SPI_MASTER_BASE, &data, &uc_pcs);

    return reverse[data & 0xFF];
}

void m_aci_q_flush(void)
{
    cpu_irq_disable();

    /* re-initialize aci cmd queue and aci event queue to flush them*/
    m_aci_q_init(&aci_tx_q);
    m_aci_q_init(&aci_rx_q);

    cpu_irq_enable();
}

void m_aci_pins_set(aci_pins_t *a_pins_ptr)
{
    a_pins_local_ptr = a_pins_ptr;	
}
