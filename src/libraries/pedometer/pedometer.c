#include "pedometer.h"
#include <compiler.h>

int g_nSteps = 1;

int filter_v[10];
int n_v = 0;
int i_v = 0;

int avg_filter(int v, int n)
{
    int i, sum = 0;

    if (n > 10)
        n = 10;
    
    filter_v[i_v] = v;

    if (++i_v >= n)
        i_v = 0;

    if (++n_v >= n)
        n_v = 10;

    for (i = 0; i < n_v; i++)
        sum += filter_v[i];

    return sum / n_v;
}

enum {
    STEP_SYNCING,
    STEP_WALKING,
};

enum {
    DIR_UP,
    DIR_DOWN,
};

enum {
    STATE_MIN,
    STATE_MAX,
    STATE_GOING_UP,
    STATE_GOING_DOWN,
};

int step_state = STEP_SYNCING;
int FSM_state = STATE_GOING_UP;
int cur_dir = DIR_UP;

typedef struct
{
    int ts;
    int value;
    int ampli;
} localminmax_t;

localminmax_t loc_min[1024] = { { 0, 0, 0} };
int lmi = 0;
const int N_SYNCING = 3;

int FSM_input(int v, int ticks)
{
    static int last_v = 0;
    static int last_max = 100;
    static int last_period = 0;
    
    switch (FSM_state)
    {
        case STATE_GOING_UP:
            if (v < last_v)
            {
                FSM_state = STATE_GOING_DOWN;
                last_max = last_v;
            }

            last_v = v;
            break;

        case STATE_GOING_DOWN:
            if (v > last_v)
            {
                /* New local mimimum.  */
                FSM_state = STATE_GOING_UP;

                /* Filter out false local minimums.  */
                if ((last_max - last_v) < 10)
                    break;
                
                switch (step_state)
                {
                    case STEP_SYNCING:
                        if (ticks - loc_min[lmi].ts > 7 && ticks - loc_min[lmi].ts < 30 &&
                            (lmi == 0 ||
                             ticks + loc_min[lmi - 1].ts - 2 * loc_min[lmi].ts < 3 &&
                             abs(loc_min[lmi].ampli - (last_max - last_v)) < 10) )
                            ++lmi;
                        else
                            lmi = 0;
                        
                        loc_min[lmi].ts = ticks;
                        loc_min[lmi].value = last_v;
                        loc_min[lmi].ampli = last_max - last_v;
                        
                        printf("%d: %d %d %d\n", ticks, last_v, lmi, step_state);
                
                        if (lmi == 3)
                        {
                            printf("%d, %d, %d\n", loc_min[lmi].ts - loc_min[lmi-1].ts,
                                   loc_min[lmi-1].ts - loc_min[lmi-2].ts,
                                   loc_min[lmi-2].ts - loc_min[lmi-3].ts);
                            
                            printf("%d, %d, %d, %d\n", loc_min[lmi].ampli,
                                   loc_min[lmi-1].ampli, loc_min[lmi-2].ampli,
                                   loc_min[lmi-3].ampli);

                            last_period = (loc_min[lmi].ts - loc_min[0].ts) / 3;

                            printf("%d\n", last_period);
                            
                            step_state = STEP_WALKING;
                            g_nSteps += 3;
                        }
                        break;

                    case STEP_WALKING:
                        if (abs(ticks - loc_min[lmi].ts - last_period) < 10)
                        {
                            last_period = (3 * last_period + ticks - loc_min[lmi].ts) / 4;

                            printf("WALKING: %d, %d, %d\n", ticks - loc_min[lmi].ts,
                                   last_period, last_max - last_v);

                            loc_min[lmi].ts = ticks;
                            loc_min[lmi].value = last_v;
                            loc_min[lmi].ampli = last_max - last_v;

                            g_nSteps++;
                        }
                        else
                        {
                            step_state = STEP_SYNCING;
                            loc_min[0] = loc_min[lmi];
                            last_period = 0;
                            lmi = 0;
                        }
                }
            }

            last_v = v;
            break;
    }
}

            
