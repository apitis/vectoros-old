#include <asf.h>
#include <conf_board.h>
#include "i2c.h"
#include "MPU9150.h"

// I2C address 0x69 could be 0x68 depends on your wiring. 
int MPU9150_I2C_ADDRESS = 0x68;

//Variables where our values can be stored
int cmps[3];
int16_t accl[3];
int16_t gyro[3];
int16_t temp;


////////////////////////////////////////////////////////////
///////// I2C functions to get easier all values ///////////
////////////////////////////////////////////////////////////

int MPU9150_readSensorHL(int addrL, int addrH)
{
    int tmp = 0;
    uint8_t *p = (uint8_t *)&tmp;

    i2c_read(TWI0, MPU9150_I2C_ADDRESS, addrL, p);
    i2c_read(TWI0, MPU9150_I2C_ADDRESS, addrH, p + 1);

    return tmp;
}

int MPU9150_readSensor(int addr)
{
    uint8_t tmp = 0;
    i2c_read(TWI0, MPU9150_I2C_ADDRESS, addr, &tmp);

    return tmp;
}

int MPU9150_writeSensor(int addr,int data)
{
    i2c_write_byte(TWI0, MPU9150_I2C_ADDRESS, addr, data);

    return 1;
}

void MPU9150_setup()
{      
    /* The MPU-9150 can be put into Accelerometer Only Low Power Mode using the following steps: 
       (i) Set CYCLE bit to 1 
       (ii) Set SLEEP bit to 0 
       (iii) Set TEMP_DIS bit to 1 
       (iv) Set STBY_XG, STBY_YG, STBY_ZG bits to 1
       (v) Set WAKE_CTRL to 2 = 20Hz*/
    MPU9150_writeSensor(MPU9150_PWR_MGMT_1, 0x28);
    MPU9150_writeSensor(MPU9150_PWR_MGMT_2, 0x87);

//  MPU9150_setupCompass();
}


void MPU9150_readXYZ()
{
  // Print all sensor values which the sensor provides
  // Formated all values as x, y, and z in order for
  // Compass, Gyro, Acceleration. The First value is
  // the temperature.

  /* double dT = ( (double) MPU9150_readSensor(MPU9150_TEMP_OUT_L,MPU9150_TEMP_OUT_H) + 12412.0) / 340.0; */
  /* MPU9150_readSensor(MPU9150_CMPS_XOUT_L,MPU9150_CMPS_XOUT_H); */
  /* MPU9150_readSensor(MPU9150_CMPS_YOUT_L,MPU9150_CMPS_YOUT_H); */
  /* MPU9150_readSensor(MPU9150_CMPS_ZOUT_L,MPU9150_CMPS_ZOUT_H); */
  /* MPU9150_readSensor(MPU9150_GYRO_XOUT_L,MPU9150_GYRO_XOUT_H); */
  /* MPU9150_readSensor(MPU9150_GYRO_YOUT_L,MPU9150_GYRO_YOUT_H); */
  /* MPU9150_readSensor(MPU9150_GYRO_ZOUT_L,MPU9150_GYRO_ZOUT_H); */
  accl[0] = MPU9150_readSensorHL(MPU9150_ACCEL_XOUT_L,MPU9150_ACCEL_XOUT_H);
  accl[1] = MPU9150_readSensorHL(MPU9150_ACCEL_YOUT_L,MPU9150_ACCEL_YOUT_H);
  accl[2] = MPU9150_readSensorHL(MPU9150_ACCEL_ZOUT_L,MPU9150_ACCEL_ZOUT_H);
}

/* Not tested yet.  */
void MPU9150_setupCompass(){
  MPU9150_I2C_ADDRESS = 0x0C;      //change Adress to Compass

  MPU9150_writeSensor(0x0A, 0x00); //PowerDownMode
  MPU9150_writeSensor(0x0A, 0x0F); //SelfTest
  MPU9150_writeSensor(0x0A, 0x00); //PowerDownMode

  MPU9150_I2C_ADDRESS = 0x69;      //change Adress to MPU

  MPU9150_writeSensor(0x24, 0x40); //Wait for Data at Slave0
  MPU9150_writeSensor(0x25, 0x8C); //Set i2c address at slave0 at 0x0C
  MPU9150_writeSensor(0x26, 0x02); //Set where reading at slave 0 starts
  MPU9150_writeSensor(0x27, 0x88); //set offset at start reading and enable
  MPU9150_writeSensor(0x28, 0x0C); //set i2c address at slv1 at 0x0C
  MPU9150_writeSensor(0x29, 0x0A); //Set where reading at slave 1 starts
  MPU9150_writeSensor(0x2A, 0x81); //Enable at set length to 1
  MPU9150_writeSensor(0x64, 0x01); //overvride register
  MPU9150_writeSensor(0x67, 0x03); //set delay rate
  MPU9150_writeSensor(0x01, 0x80);

  MPU9150_writeSensor(0x34, 0x04); //set i2c slv4 delay
  MPU9150_writeSensor(0x64, 0x00); //override register
  MPU9150_writeSensor(0x6A, 0x00); //clear usr setting
  MPU9150_writeSensor(0x64, 0x01); //override register
  MPU9150_writeSensor(0x6A, 0x20); //enable master i2c mode
  MPU9150_writeSensor(0x34, 0x13); //disable slv4
}

